'use strict';

// Required modules
let fs = require('fs');
let async = require('async');
let sqlite3 = require('sqlite3');

let common = require('../lib/common');

/**
 * The object to be exported.
 */
let Db = function () {
};

Db.prototype.BAR_COLUMN = {
    TIMESTAMP: 0,
    OPEN: 1,
    HIGH: 2,
    LOW: 3,
    CLOSE: 4,
    SIZE: 5,
    LAST_SIZE: 6,
    SPREAD_HIGH: 7,
    SPREAD_LOW: 8,
    UNDERLIER: 1,
    INTEREST_RATE: 2,
    DELTA: 3,
    GAMMA: 4
};

Db.prototype.TICK_COLUMN = {
    TYPE: 0,
    TIMESTAMP: 1,
    T_PRICE: 2,
    T_SIZE: 3,
    Q_BID: 2,
    Q_ASK: 3
};

/**
 * The maximum number of lines in a single file. These are rather a guidance than a hard limit.
 */
Db.prototype.TICK_FILE_MAX_LINES = 10000;
Db.prototype.BAR_FILE_MAX_LINES = 5000;

/**
 * Get the path of the tick file specified by the given parameters.
 */
Db.prototype.getTickFilePath = function (symbol, underlier, index, start, check) {
    let path = common.getDbRoot(common.DATA_TYPE.TICK, check);

    path += underlier + common.getPathSeparator();
    if (check) {
        common.checkDir(path);
    }

    path += start.toString().substring(0, 6) + common.getPathSeparator();
    if (check) {
        common.checkDir(path);
    }

    path += symbol + '_' + index + '.csv';

    return path;
};

/**
 * Get the path of the bar file specified by the given parameters.
 */
Db.prototype.getBarFilePath = function (symbol, underlier, bartype, index, start, check) {
    let path = common.getDbRoot(bartype, check);

    path += underlier + common.getPathSeparator();
    if (check) {
        common.checkDir(path);
    }

    path += start.toString().substring(0, 6) + common.getPathSeparator();
    if (check) {
        common.checkDir(path);
    }

    path += symbol + '_' + index + '.csv';

    return path;
};

/**
 * SQL statements to read/write tick lookup DB.
 */
Db.prototype.SELECT_NEWEST_TICK = 'SELECT * FROM ticks WHERE symbol=? ORDER BY start DESC LIMIT 1';
Db.prototype.SELECT_OLDEST_TICK = 'SELECT * FROM ticks WHERE symbol=? ORDER BY start ASC LIMIT 1';
Db.prototype.SELECT_TICKS = 'SELECT * FROM ticks WHERE symbol=? AND end>=? ORDER BY start ASC';
Db.prototype.INSERT_TICK = 'INSERT INTO ticks VALUES(?,?,?,?,?)';
Db.prototype.UPDATE_TICK = 'UPDATE ticks SET end=?,count=? WHERE symbol=? AND fileId=?';

/**
 * Get the handle of the database for tick data.
 */
let tickerLookupDb;
Db.prototype.getTickerLookupDb = function (callback) {
    if (!tickerLookupDb) {
        let path = common.getDbRoot(common.DATA_TYPE.TICK, true) + 'lookup.sqlite';
        tickerLookupDb = new sqlite3.Database(path);
        tickerLookupDb.configure("busyTimeout", 10000);
        let SEE_IF_TICK_TABLE_EXISTS = 'SELECT name FROM sqlite_master WHERE name="ticks"';
        tickerLookupDb.get(SEE_IF_TICK_TABLE_EXISTS, [], function (err, row) {
            if (err) {
                tickerLookupDb.close();
                return callback(err, null);
            }

            // If the table doesn't exist, create it here.
            if (!row) {
                let CREATE_TICK_TABLE = 'CREATE TABLE ticks (symbol text,start integer,end integer,count integer,fileId integer)';
                let CREATE_TICK_SYMBOL_INDEX = 'CREATE INDEX IF NOT EXISTS symbol_index on ticks (symbol)';
                let CREATE_TICK_START_INDEX = 'CREATE INDEX IF NOT EXISTS start_index on ticks (start)';
                let CREATE_TICK_FILEID_INDEX = 'CREATE INDEX IF NOT EXISTS fileid_index on ticks (fileId)';
                tickerLookupDb.serialize(function () {
                    tickerLookupDb.run(CREATE_TICK_TABLE);
                    tickerLookupDb.run(CREATE_TICK_SYMBOL_INDEX);
                    tickerLookupDb.run(CREATE_TICK_FILEID_INDEX);
                    tickerLookupDb.run(CREATE_TICK_START_INDEX, function (err) {
                        return callback(err, tickerLookupDb);
                    });
                });
            }
            else {
                return callback(null, tickerLookupDb);
            }
        });
    }
    else {
        return async.setImmediate(function () {
            callback(null, tickerLookupDb)
        });
    }
};

/**
 * SQL statements to read/write tick update DB.
 */
Db.prototype.SELECT_TICK_UPDATE = 'SELECT * FROM tick_updates WHERE symbol=?';
Db.prototype.INSERT_TICK_UPDATE = 'INSERT INTO tick_updates VALUES(?,?)';
Db.prototype.UPDATE_TICK_UPDATE = 'UPDATE tick_updates SET lastUpdate=? WHERE symbol=?';

/**
 * Get the handle of the database that keeps track of when the ticker for a symbol is updated last time. This information is used to reduce unnecessary
 * server query.
 */
let tickerUpdateDb;
Db.prototype.getTickerUpdateDb = function (callback) {
    if (!tickerUpdateDb) {
        let path = common.getDbRoot(common.DATA_TYPE.TICK, true) + 'update.sqlite';
        tickerUpdateDb = new sqlite3.Database(path);
        tickerUpdateDb.configure("busyTimeout", 10000);
        let SEE_IF_TICK_UPDATE_TABLE_EXISTS = 'SELECT name FROM sqlite_master WHERE name="tick_updates"';
        tickerUpdateDb.get(SEE_IF_TICK_UPDATE_TABLE_EXISTS, [], function (err, row) {
            if (err) {
                tickerUpdateDb.close();
                return callback(err, null);
            }

            // If the table doesn't exist, create it here.
            if (!row) {
                let CREATE_TICK_UPDATE_TABLE = 'CREATE TABLE tick_updates (symbol text,lastUpdate integer)';
                let CREATE_TICK_UPDATE_SYMBOL_INDEX = 'CREATE UNIQUE INDEX IF NOT EXISTS symbol_index on tick_updates (symbol)';
                tickerUpdateDb.serialize(function () {
                    tickerUpdateDb.run(CREATE_TICK_UPDATE_TABLE);
                    tickerUpdateDb.run(CREATE_TICK_UPDATE_SYMBOL_INDEX, function (err) {
                        return callback(err, tickerUpdateDb);
                    });
                });
            }
            else {
                return callback(null, tickerUpdateDb);
            }
        });
    }
    else {
        return async.setImmediate(function () {
            callback(null, tickerUpdateDb)
        });
    }
};

/**
 * SQL statements to read/write bar lookup DB.
 */
Db.prototype.SELECT_NEWEST_BAR = 'SELECT * FROM bars WHERE symbol=? AND resolution=? ORDER BY start DESC LIMIT 1';
Db.prototype.SELECT_OLDEST_BAR = 'SELECT * FROM bars WHERE symbol=? AND resolution=? ORDER BY start ASC LIMIT 1';
Db.prototype.SELECT_BARS = 'SELECT * FROM bars WHERE symbol=? AND resolution=? AND end>=? AND start<=? ORDER BY start ASC';
Db.prototype.SELECT_BARS_WITH_ALL_RESOLUTIONS = 'SELECT * FROM bars WHERE symbol=? AND end>=? ORDER BY start ASC';
Db.prototype.INSERT_BAR = 'INSERT INTO bars VALUES(?,?,?,?,?)';
Db.prototype.UPDATE_BAR = 'UPDATE bars SET end=? WHERE symbol=? AND resolution=? AND fileId=?';
Db.prototype.DELETE_BAR = 'DELETE FROM bars WHERE symbol=? AND resolution=? AND fileId=?';

Db.prototype.SELECT_DISTINCT_UNDERLIERS = 'SELECT DISTINCT symbol FROM bars WHERE symbol LIKE ? AND resolution=? AND end>=? AND start<=? ORDER BY symbol';

/**
 * Get the handle of the database for bar data.
 */
let barLookupDb;
Db.prototype.getBarLookupDb = function (callback) {
    if (!barLookupDb) {
        let path = common.getDbRoot(common.DATA_TYPE.BARS, true) + 'lookup.sqlite';
        barLookupDb = new sqlite3.Database(path);
        barLookupDb.configure("busyTimeout", 10000);
        let SEE_IF_BAR_TABLE_EXISTS = 'SELECT name FROM sqlite_master WHERE name="bars"';
        barLookupDb.get(SEE_IF_BAR_TABLE_EXISTS, [], function (err, row) {
            if (err) {
                barLookupDb.close();
                return callback(err, null);
            }

            // If the table doesn't exist, create it here.
            if (!row) {
                let CREATE_BAR_TABLE = 'CREATE TABLE bars (symbol text,resolution integer,start integer,end integer,fileId integer)';
                let CREATE_BAR_SYMBOL_INDEX = 'CREATE INDEX IF NOT EXISTS symbol_index on bars (symbol)';
                let CREATE_BAR_RESOLUTION_INDEX = 'CREATE INDEX IF NOT EXISTS resolution_index on bars (resolution)';
                let CREATE_BAR_START_INDEX = 'CREATE INDEX IF NOT EXISTS start_index on bars (start)';
                let CREATE_BAR_FILEID_INDEX = 'CREATE INDEX IF NOT EXISTS fileid_index on bars (fileId)';
                barLookupDb.serialize(function () {
                    barLookupDb.run(CREATE_BAR_TABLE);
                    barLookupDb.run(CREATE_BAR_SYMBOL_INDEX);
                    barLookupDb.run(CREATE_BAR_RESOLUTION_INDEX);
                    barLookupDb.run(CREATE_BAR_START_INDEX);
                    barLookupDb.run(CREATE_BAR_FILEID_INDEX, function (err) {
                        return callback(err, barLookupDb);
                    });
                });
            }
            else {
                return callback(null, barLookupDb);
            }
        });
    }
    else {
        return async.setImmediate(function () {
            callback(null, barLookupDb)
        });
    }
};

/**
 * SQL statements to read/write bar update DB.
 */
Db.prototype.SELECT_BAR_UPDATE = 'SELECT * FROM bar_updates WHERE symbol=?';
Db.prototype.INSERT_BAR_UPDATE = 'INSERT INTO bar_updates VALUES(?,?)';
Db.prototype.UPDATE_BAR_UPDATE = 'UPDATE bar_updates SET lastUpdate=? WHERE symbol=?';

/**
 * Get the handle of the database that keeps track of when the bars for a symbol is updated last time. This information is used to reduce unnecessary
 * server query.
 */
let barUpdateDb;
Db.prototype.getBarUpdateDb = function (callback) {
    if (!barUpdateDb) {
        let path = common.getDbRoot(common.DATA_TYPE.BARS, true) + 'update.sqlite';
        barUpdateDb = new sqlite3.Database(path);
        barUpdateDb.configure("busyTimeout", 10000);
        let SEE_IF_BAR_UPDATE_TABLE_EXISTS = 'SELECT name FROM sqlite_master WHERE name="bar_updates"';
        barUpdateDb.get(SEE_IF_BAR_UPDATE_TABLE_EXISTS, [], function (err, row) {
            if (err) {
                barUpdateDb.close();
                return callback(err, null);
            }

            // If the table doesn't exist, create it here.
            if (!row) {
                barUpdateDb.serialize(function () {
                    let CREATE_BAR_UPDATE_TABLE = 'CREATE TABLE bar_updates (symbol text,lastUpdate integer)';
                    let CREATE_BAR_UPDATE_SYMBOL_INDEX = 'CREATE UNIQUE INDEX IF NOT EXISTS symbol_index on bar_updates (symbol)';
                    barUpdateDb.run(CREATE_BAR_UPDATE_TABLE);
                    barUpdateDb.run(CREATE_BAR_UPDATE_SYMBOL_INDEX, function (err) {
                        return callback(err, barUpdateDb);
                    });
                });
            }
            else {
                return callback(null, barUpdateDb);
            }
        });
    }
    else {
        return async.setImmediate(function () {
            callback(null, barUpdateDb)
        });
    }
};

module.exports = new Db();