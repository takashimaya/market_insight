'use strict';

// Required modules
let fs = require('fs');
let os = require('os');
let http = require('http');
let process = require('process');

let logger = require('../lib/logger');

/**
 * The object to be exported.
 */
let Common = function () {
};

Common.prototype.OPTION_TYPE = {
    INVALID: 0,
    CALL: 10,
    PUT: 20
};

Common.prototype.DATA_TYPE = {
    TICK: 10,
    BARS: 15,
    _5MIN: 30,
    _30MIN: 40,
    _1DAY: 50,
    RATES: 60
};

Common.prototype.isEmpty = function (obj) {
    // http://stackoverflow.com/questions/679915/how-do-i-test-for-an-empty-javascript-object
    return Object.keys(obj).length === 0 && obj.constructor === Object;
};

/**
 * Returns the Date object corresponding to the given timestamp. Timestamp is as integer with one of the following
 * formats 'YYYYMMDDHHMMSSmmm' or 'YYYYMMDDHHMMSS'.
 */
Common.prototype.timestampToDate = function (timestamp) {
    let str = timestamp.toString();

    let y = parseInt(str.substring(0, 4));
    let m = parseInt(str.substring(4, 6));
    let d = parseInt(str.substring(6, 8));
    let h = parseInt(str.substring(8, 10));
    let min = parseInt(str.substring(10, 12));
    let s = parseInt(str.substring(12, 14));

    let millisec = 0;
    if (str.length == 17) {
        millisec = parseInt(str.substring(14, 17));
    }

    return new Date(y, m - 1, d, h, min, s, millisec);
};

/**
 * Create a timestamp from 'date' (javascript Date object).
 */
Common.prototype.dateToTimestamp = function (date, fixedTime, millisec) {
    let str = date.getFullYear().toString();
    str += Common.prototype.padInt(date.getMonth() + 1, 2);
    str += Common.prototype.padInt(date.getDate(), 2);
    if (fixedTime) {
        if (fixedTime === -1) {
            return str;
        }

        str += fixedTime;
    }
    else {
        str += Common.prototype.padInt(date.getHours(), 2);
        str += Common.prototype.padInt(date.getMinutes(), 2);
        str += Common.prototype.padInt(date.getSeconds(), 2);

        if (millisec) {
            str += Common.prototype.padInt(date.getMilliseconds(), 3);
        }
    }

    return str;
};

/**
 * Take a timestamp with the long format and convert it to the short format by ignoring millisecond part.
 */
Common.prototype.truncateMilliseconds = function (timestamp) {
    return Math.floor((parseInt(timestamp) / 1000));
};

/**
 * Send HTTP request to ActiveTick server proxy.
 */
let trial = 0;
Common.prototype.sendHttpRequest = function (command, query, callback) {
    let recover = function () {
        if (trial > 50) {
            logger.add(logger.TYPE.ERROR, 'Activetick', 'Server not working. Terminated.');
            return callback('Server not responding.');
        }

        logger.add(logger.TYPE.ERROR, 'Activetick', 'Server timeout. Resetting...');
        var exec = require('child_process').exec;
        var kill = 'kill -9 $(lsof -i :3200|grep ActiveTi|cut -d" " -f2)';
        exec(kill, function (error, stdout, stderr) {
            var serverDir = __dirname + Common.prototype.getPathSeparator() + '..' + Common.prototype.getPathSeparator() + 'activetick_server';
            var serverExePath = '';
            if (Common.prototype.isWindows()) {
                serverDir += Common.prototype.getPathSeparator() + 'windows';
                serverExePath = serverDir + common.getPathSeparator() + 'ActiveTickFeedHttpServer.exe';
            }
            else {
                serverDir += Common.prototype.getPathSeparator() + 'mac';
                serverExePath = serverDir + Common.prototype.getPathSeparator() + 'ActiveTickFeedHttpServer';
            }

            const spawn = require('child_process').spawn;
            const activeTick = spawn(serverExePath,
                                     [ '127.0.0.1', '3200', 'activetick1.activetick.com', '5cebbc79903749c2bba2d6ecf6c8574c', 'ttsuboi', '0r30r3sag1' ],
                                     { cwd: serverDir });

            activeTick.stdout.on('data', (data) => {
                console.log(`stdout: ${data}`);
            });

            activeTick.stderr.on('data', (data) => {
                console.log('> ActiveTickServer ERROR: ' + `stderr: ${data}`);
            });

            activeTick.on('close', (code) => {
                console.log(`child process exited with code ${code}`);
            });

            return setTimeout(function () {
                return Common.prototype.sendHttpRequest(command, query, callback);
            }, 30000);
        });
    };

    let options = {
        hostname: 'localhost',
        port: 3200,
        path: '/' + command + '?' + query,
        method: 'GET',
    };

    ++trial;
    http.get(options, function (response) {
        let body = '';
        response.on('data', function (data) {
            body += data;
        });
        response.on('end', function () {
            trial = 0;
            return callback(null, body);
        });
    }).on('error', function (err) {
        logger.add(logger.TYPE.ERROR, 'Activetick', err);
        return recover();
    }).setTimeout(60000, function () {
        return recover();
    });
};

/**
 * Returns the name of the machine the program is currently running on.
 */
Common.prototype.machineName = function () {
    return os.hostname();
};

/**
 * Returns true if the program is running on Windows.
 */
Common.prototype.isWindows = function () {
    return /^win/.test(process.platform);
};

/**
 * Returns the path separator - '/' for Mac/Linux and '\\' for Windows
 */
Common.prototype.getPathSeparator = function () {
    if (this.isWindows()) {
        return "\\";
    }
    else {
        return "/";
    }
};

/**
 * Returns the path of ticker/bar data.
 */
Common.prototype.getDbPath = function (type) {
    if (type === Common.prototype.DATA_TYPE.TICK) {
        if (this.isWindows()) {
            return "D:\\\\MarketInsight_DB\\";
        }
        else {
            return "/Volumes/Ext/MarketInsight_DB/";
        }
    }
    else {
        if (this.isWindows()) {
            return "D:\\\\Dropbox\\Investment\\MarketInsight_DB\\";
        }
        else {
            return "/Users/Takashi/Dropbox/Investment/MarketInsight_DB/";
        }
    }
};

/**
 * Returns the path of Dropbox folder
 */
Common.prototype.getDropboxPath = function () {
    if (this.isWindows()) {
        return "D:\\\\Dropbox\\Investment\\Data\\";
    }
    else {
        return "/Users/Takashi/Dropbox/Investment/Data/";
    }
};

/**
 * Return the root path of the data files, which depends on the data 'type'. If 'check' is true, check if the directory
 * exists and if not create it.
 */
Common.prototype.getDbRoot = function (type, check) {
    let path = Common.prototype.getDbPath(type);

    if (check) {
        Common.prototype.checkDir(Common.prototype.getDbPath(type));
    }

    if (type === Common.prototype.DATA_TYPE.TICK) {
        path += 'ticks' + Common.prototype.getPathSeparator();
    }
    else if (type === Common.prototype.DATA_TYPE.RATES) {
        path += 'rates' + Common.prototype.getPathSeparator();
    }
    else {
        path += 'bars' + Common.prototype.getPathSeparator();
        if (check) {
            Common.prototype.checkDir(path);
        }

        if (type === Common.prototype.DATA_TYPE._5MIN) {
            path += '_5min' + Common.prototype.getPathSeparator();
        }
        else if (type === Common.prototype.DATA_TYPE._30MIN) {
            path += '_30min' + Common.prototype.getPathSeparator();
        }
        else if (type === Common.prototype.DATA_TYPE._1DAY) {
            path += '_1day' + Common.prototype.getPathSeparator();
        }
    }

    if (check) {
        Common.prototype.checkDir(path);
    }

    return path;
};

/**
 * Returns the path of the folder used for data exchange between master and slave.
 */
Common.prototype.getDataExchangePath = function () {
    if (this.isWindows()) {
        return "C:\\\\Users\\ttsuboi\\Dropbox\\Investment\\MarketInsight_DX";
    }
    else {
        return "/Users/Takashi/Dropbox/Investment/MarketInsight_DX";
    }
};

/**
 * Create a directory if it does not exist.
 */
Common.prototype.mkdirSync = function (path) {
    try {
        fs.mkdirSync(path);
    }
    catch (e) {
        if (e.code !== 'EEXIST') {
            throw e;
        }
    }
};

/**
 * Returns true if the specified directory exists.
 */
Common.prototype.dirExists = function (path) {
    try {
        let stats = fs.lstatSync(path);
        if (stats.isDirectory()) {
            return true;
        }
    }
    catch (e) {
    }
    return false;
};

/**
 * See if the directory exists. If not, create it.
 */
Common.prototype.checkDir = function (path) {
    if (!this.dirExists(path)) {
        this.mkdirSync(path);
    }
};

/**
 * Returns true if the specified file exists.
 */
Common.prototype.fileExists = function (path) {
    try {
        let stats = fs.lstatSync(path);
        if (stats.isFile()) {
            return true;
        }
    }
    catch (e) {
    }
    return false;
};

/**
 * Returns the modified time for the specified file.
 */
Common.prototype.getModifiedTime = function (path) {
    var stats = fs.statSync(path);
    return new Date(util.inspect(stats.mtime));
};

/**
 * Returns a zero padded integer as a string value.
 */
Common.prototype.padInt = function (value, digits) {
    let padding = '0'.repeat(digits);
    return (padding + value).substr(-1 * digits, digits);
};

/**
 * Take a float which represents $$ amount and returns the corresponding integer after multiplying x100 and rounding it.
 */
Common.prototype.convertCurrencyToInt = function (moneyStr) {
    return (moneyStr === '') ? 0 : Math.round(parseFloat(moneyStr) * 100);
};

let optionRegExp = /^(\D*)([0-9]{2})([0-9]{2})([0-9]{2})([C|P])(\S*)/;

/**
 * Parse an option symbol following the format of 'AVGO160422C155' and returns the components as follows:
 *   {
 *      underlier: 'AVGO',
 *      expireDate: new Date(2016, 03, 22),
 *      optionType: OPTION_TYPE.CALL,
 *      strike: 15500
 *   }
 */
Common.prototype.parseOptionSymbol = function (symbol) {
    let regRes = optionRegExp.exec(symbol);
    if (!regRes) {
        return null;
    }

    let year = parseInt('20' + regRes[ 2 ]);
    let month = parseInt(regRes[ 3 ]);
    let day = parseInt(regRes[ 4 ]);
    let date = new Date(year, month - 1, day);

    let optionType;
    if (regRes[ 5 ] === 'C') {
        optionType = Common.prototype.OPTION_TYPE.CALL;
    }
    else if (regRes[ 5 ] === 'P') {
        optionType = Common.prototype.OPTION_TYPE.PUT;
    }
    else {
        throw new Error('ERROR: unknown option type found in "' + symbol + '"');
    }

    return {
        underlier: regRes[ 1 ],
        expireDate: date,
        optionType: optionType,
        strike: this.convertCurrencyToInt(regRes[ 6 ])
    };
};

Common.prototype.moreDataSinceLastUpdate = function (lastUpdate, option, futures) {
    // Disable it for now. The problem is that accessing ActiveTick at a certain point in time does not
    // guarantee we get updated up to that point. We need to check the updated contents and see if we are
    // really updated or not.
    return true;

    let lastUpdateDate = lastUpdate.getDate();

    let now = new Date();
    let day = now.getDay();
    let hour = now.getHours();
    let min = now.getMinutes();

    let lastMarketClose = new Date(now);

    if (futures) {
        return true;
        // Saturday or Sunday (before 3 pm)
        if ((day === 6) || ((day === 0) && (hour < 15))) {
            // The market closed at 14:00 PST/PDT on Friday
            lastMarketClose.setHours(14, 0, 0, 0);
            lastMarketClose.setDate(lastUpdateDate - (day === 6) ? 1 : 2);
        }
    }
    else {
        // Saturday or Sunday
        if ((day === 6) || (day === 0)) {
            if (option) {
                // Options are traded only during the regular market hours - from 6:30 am to 1 pm PST/PDT.
                lastMarketClose.setHours(13, 0, 0, 0);
            }
            else {
                // Suppose underliers are traded in extended hours - from 1:30 am to 5 pm PST/PDT.
                // We have an arbitrary shortcut - from 5 am to 3 pm PST/PDT
                lastMarketClose.setHours(15, 0, 0, 0);
            }

            // Move back to last Friday.
            lastMarketClose.setDate(lastUpdateDate - (day === 6) ? 1 : 2);
        }
        else {
            if (option) {
                // Options are traded only during the regular market hours - from 6:30 am to 1 pm PST/PDT.
                lastMarketClose.setHours(13, 0, 0, 0);
                if ((hour < 6) || ((hour === 6) && (min < 30))) {
                    // The market closed at 1 pm PST/PDT on the previous day.
                    lastMarketClose.setDate(lastUpdateDate - 1);
                }
            }
            else {
                // Suppose underliers are traded in extended hours - from 1:30 am to 5 pm PST/PDT.
                // We have an arbitrary shortcut - from 5 am to 3 pm PST/PDT
                lastMarketClose.setHours(15, 0, 0, 0);
                if ((hour < 6) || ((hour === 6) && (min < 30))) {
                    // The market closed at 3 pm PST/PDT on the previous day.
                    lastMarketClose.setDate(lastUpdateDate - 1);
                }
            }
        }
    }

    return (lastUpdate.getTime() < lastMarketClose.getTime());
};

Common.prototype.isMarketOpen = function (date) {
    let y = date.getFullYear();
    let m = date.getMonth() + 1;
    let d = date.getDay();
    let h = date.getHours();

    // New Year's Day
    if ((m === 1) && (d === 1)) {
        return false;
    }
    if ((y === 2017) && (m === 1) && (d === 2)) {
        return false;
    }

    // Martin Luther King Jr. Day
    if ((y === 2017) && (m === 1) && (d === 16)) {
        return false;
    }
    if ((y === 2018) && (m === 1) && (d === 15)) {
        return false;
    }

    // President's Day
    if ((y === 2017) && (m === 2) && (d === 20)) {
        return false;
    }
    if ((y === 2018) && (m === 2) && (d === 19)) {
        return false;
    }

    // Good Friday
    if ((y === 2017) && (m === 4) && (d === 14)) {
        return false;
    }
    if ((y === 2018) && (m === 3) && (d === 30)) {
        return false;
    }

    // Memorial Day
    if ((y === 2017) && (m === 5) && (d === 29)) {
        return false;
    }
    if ((y === 2018) && (m === 5) && (d === 28)) {
        return false;
    }

    // Independence Day
    if ((m === 7) && (d === 4)) {
        return false;
    }
    if ((m === 7) && (d === 3) && (h > 10)) {
        return false;
    }

    // Labor Day
    if ((y === 2017) && (m === 9) && (d === 4)) {
        return false;
    }
    if ((y === 2018) && (m === 9) && (d === 3)) {
        return false;
    }

    // Thanksgiving Day
    if ((y === 2017) && (m === 11) && (d === 23)) {
        return false;
    }
    if ((y === 2017) && (m === 11) && (d === 24) && (h > 10)) {
        return false;
    }
    if ((y === 2018) && (m === 11) && (d === 22)) {
        return false;
    }
    if ((y === 2018) && (m === 11) && (d === 23) && (h > 10)) {
        return false;
    }

    // Christmas
    if ((m === 12) && (d === 25)) {
        return false;
    }
    if ((y === 2018) && (m === 12) && (d === 24) && (h > 10)) {
        return false;
    }

    let day = date.getDay();
    if ((1 <= day) && (day <= 5)) {
        if ((6 <= h) && (h <= 13)) {
            let min = date.getMinutes();
            if ((6 === h) && (min < 30)) {
                return false;
            }
            return true;
        }
    }
    return false;
};

module.exports = new Common();