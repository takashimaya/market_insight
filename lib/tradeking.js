'use strict';

// Required modules
let request = require('request');
let async = require('async');
let common = require('../lib/common');
let logger = require('../lib/logger');

/**
 * The object to be exported.
 */
let TradeKing = function () {
};

// OAuth key
let oauth = {
    consumer_key: 'FIojyju7i1xWofnwUGXTX0mqDiM2I60yrTYNiX7j3eE2',
    consumer_secret: 'CYfRR8Y166XOSruIgesYLKLVTERnnSajjaf2lo6ppXE6',
    token: 'h0HRYDmkzNkvdnxnbDHSkQNc2LWF0bMm2b5QIp3AZ6g2',
    token_secret: 'Ds29oBThaRAj7gE3K6YMIjh2KpKW5ZUycJE0j3NiR6A0'
};

/*
 * Send a request to the server. If the request violates rate limits, it will be resent after a while.
 * Also if error is returned by the server, the request will be resent up to 10 times. Finally, callback
 * function is invoked with the result, which is the contents of response in json format.
 */
let sendRequest = function (url, method, timeout, callback) {
    let json = '';
    let trial = 0;

    let send = function () {
        let param = {
            url: 'https://api.tradeking.com/v1/' + url,
            oauth: oauth
        };
        let responseFunc = function (err, response, body) {
            if (err) {
                return callback(err);
            }

            // Limit Expire seems to show 5 hours ahead of the current time... Useless.
            // let limitExpire = response.headers[ 'x-ratelimit-expire' ];
            // let currentTime = new Date(response.headers[ 'date' ]).getTime() / 1000;

            // Let's just check 'x-ratelimit-remaining'. This value seems to get reset back to 60 every 60 seconds.
            let r = response.headers[ 'x-ratelimit-remaining' ];
            if (r) {
                let rateLimit = parseInt(r);
                // console.log('Rate limit :' + rateLimit);
                if (rateLimit < 5) {
                    console.log('TradeKing Request Throttling...');
                    return setTimeout(function () {
                        send();
                    }, 1000 * (10 - rateLimit));
                }
            }

            try {
                json = JSON.parse(body);
            }
            catch (exp) {
                ++trial;
                if (trial < 10) {
                    logger.add(logger.TYPE.ERROR, 'TradeKing', 'Server request failed. Try again... ');
                    return setTimeout(function () {
                        send();
                    }, 15000);
                }
                else {
                    logger.add(logger.TYPE.ERROR, 'TradeKing', 'Server doesn\'t respond correctly. Respose: ' + body);
                    return callback(new Error('Server request error.'));
                }
            }

            if (json.response.message) {
                return callback(new Error('Error: ' + json.response.message));
            }
            return callback(null, json.response);
        };

        if (method === 'POST') {
            request.post(param, responseFunc);
        }
        else if (method === 'GET') {
            request.get(param, responseFunc);
        }
    };

    setTimeout(function () {
        send();
    }, timeout);
};

TradeKing.prototype.getOptionChain = function (symbol, callback) {
    let optionChain = [];

    async.parallel([ function (asyncCallback) {
        // Retrieve the list of strike prices available for the symbol
        return sendRequest('market/options/strikes.json?symbol=' + symbol, 'GET', 100, function (err, response) {
            if (err) {
                return asyncCallback(err);
            }
            return asyncCallback(null, response.prices.price);
        });
    }, function (asyncCallback) {
        // Retrieve the list of expiration dates available for the symbol
        return sendRequest('market/options/expirations.json?symbol=' + symbol, 'GET', 100, function (err, response) {
            if (err) {
                return asyncCallback(err);
            }
            return asyncCallback(null, response.expirationdates.date);
        });
    } ], function (err, results) {
        if (err) {
            return callback(err);
        }

        let strikes = [];
        let strikeLen = results[ 0 ].length;
        for (let i = 0; i < strikeLen; ++i) {
            let strike = results[ 0 ][ i ];

            let dollars = '';
            let cents = '';

            let floatingPoint = strike.indexOf('.');
            if (floatingPoint >= 0) {
                dollars = strike.substring(0, floatingPoint);
                cents = strike.substring(floatingPoint + 1);
            }
            else {
                dollars = strike;
            }

            dollars = common.padInt(dollars, 5);
            cents = (cents + '000').substring(0, 3);

            strikes.push(dollars + cents);
        }

        let expires = [];
        let expireLen = results[ 1 ].length;
        for (let j = 0; j < expireLen; ++j) {
            let expire = results[ 1 ][ j ];

            let yearStr = expire.substring(2, 4);
            let monthStr = expire.substring(5, 7);
            let dayStr = expire.substring(8, 10);

            expire = yearStr + monthStr + dayStr;
            expires.push(expire);
        }

        symbol = (symbol + '-------').substring(0, 6);

        // Create the list of option symbols for which the data will be requested.
        for (let i = 0; i < strikes.length; ++i) {
            let strike = strikes[ i ];
            for (let j = 0; j < expires.length; ++j) {
                let expire = expires[ j ];
                optionChain.push('OPTION:' + symbol + expire + 'C' + strike);
                optionChain.push('OPTION:' + symbol + expire + 'P' + strike);
            }
        }

        return async.setImmediate(function () {
            let str = '';

            let len = optionChain.length;
            for (let i = 0; i < len; ++i) {
                str += optionChain[ i ];
                if (i < len - 1) {
                    str += '\r\n';
                }
            }

            return callback(null, str);
        });
    });
};

module.exports = new TradeKing();
