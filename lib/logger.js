'use strict';

// Required modules
let common = require('../lib/common');

/**
 * The object to be exported.
 */
let Logger = function () {
};

let sockets = [];

Logger.prototype.TYPE = {
    INFO: 'info',
    UPDATE: 'update',
    WARNING: 'warning',
    ERROR: 'err'
};

Logger.prototype.add = function (type, category, msg) {
    let contents = {
        timestamp: new Date().getTime(),
        type: type,
        category: category,
        msg: msg
    };

    let logMsg;
    switch (type) {
        case Logger.prototype.TYPE.INFO:
            logMsg = '[INFO] ';
            break;
        case Logger.prototype.TYPE.UPDATE:
            logMsg = '[UPDATE] ';
            break;
        case Logger.prototype.TYPE.WARNING:
            logMsg = '[WARNING] ';
            break;
        case Logger.prototype.TYPE.ERROR:
            logMsg = '[ERROR] ';
            break;
    }

    logMsg += category + ' ' + msg;

    console.log(logMsg);
    sockets.forEach(function (socket) {
        socket.emit('log', contents);
    });
};

Logger.prototype.addSocket = function (socket) {
    sockets.push(socket);
};

Logger.prototype.removeSocket = function (socket) {
    let index = sockets.indexOf(socket);
    if (index > -1) {
        sockets.splice(index, 1);
    }
};

module.exports = new Logger();