'use strict';

// Required modules
let common = require('../lib/common');
let sqlite3 = require('sqlite3');
let async = require('async');

/**
 * The object to be exported.
 */
let Fidelity = function () {
};

let POSITION_STATUS = {
    OPEN: 10,
    INACTIVE: 20,
    CLOSED: 30
};

let singletonDb;
let refCount = 0;

/**
 * Creates and returns the handle to the database. The handle is a singleton object to be shared across this library.
 * Reference counter is used to manage the lifetime of the handle. Any client who needs access to the database should
 * call openDb() and closeDb() before and after access, respectively.
 */
let openDb = function () {
    if (!singletonDb) {
        let path = common.getDropboxPath() + 'Accounts' + common.getPathSeparator() + 'accounts.sqlite';
        singletonDb = new sqlite3.Database(path);
        refCount = 1;
    }
    else {
        ++refCount;
    }
    return singletonDb;
};

/**
 * Decrement reference counter. When the counter reaches zero, close the database.
 */
let closeDb = function () {
    if (--refCount === 0) {
        singletonDb.close();
        singletonDb = null;
    }
};

/**
 * Create a SELECT statement with WHERE clause consisting of the specified conditions concatenated with AND's.
 * The conditions are given by the second parameter which is an array, e.g.:
 *
 *   ['a', 'b', 'c']
 *
 * Then, this method would create a statement:
 *
 *   SELECT * FROM tableName WHERE a=$a AND b=$b AND c=$c
 *
 * Now, the third parameter 'obj' is supposed to define $a, $b, and $c as its own properties. The method will return
 * the 'result' object whose structure is as follows:
 *
 *   result {
 *     statement: 'SELECT * FROM tableName WHERE a=$a AND b=$b AND c=$c',
 *     params: { $a: obj.$a, $b: obj,$b, $c: obj.$c }
 *   }
 *
 * 6/19/2016: Now an element in the 'conditions' can be an array of three sub-elements, e.g. ['a', 'b', ['c', '<', 'd']].
 *
 */
let buildSelectStatement = function (tableName, conditions, obj) {
    let params = {};
    let select = 'SELECT * FROM ' + tableName + ' WHERE ';

    let length = conditions.length;
    for (let i = 0; i < length; ++i) {
        if (i !== 0) {
            select += ' AND ';
        }

        let let0;
        let let1;
        let operator = '=';

        if (Array.isArray(conditions[ i ])) {
            let subElements = conditions[ i ];
            let0 = subElements[ 0 ];
            operator = subElements[ 1 ];
            let1 = subElements[ 2 ];
        }
        else {
            let0 = conditions[ i ];
            let1 = let0;
        }

        select += let0 + operator + '$' + let1;
        params[ '$' + let1 ] = obj[ '$' + let1 ];
    }

    return {
        statement: select,
        params: params
    };
};

/**
 * Return the list of underliers that have been traded in the past. If the start date is specified,
 * then only look for the positions that have been open since that date.
 */
Fidelity.prototype.getTradedUnderliers = function (startDate, callback) {
    let db = openDb();
    let select = 'SELECT DISTINCT underlier FROM positions';
    let params = [];
    if (startDate) {
        select += ' WHERE date >= ?';
        params = [ startDate.getTime() ];
    }

    db.all(select, params, function (err, rows) {
        if (err) {
            closeDb();
            return callback(err);
        }

        let underliers = [];
        rows.forEach(function (row) {
            underliers.push({ underlier: row.underlier });
        });

        select = 'SELECT * FROM positions WHERE realizedGain <> 0 AND underlier = ?';

        let currentYear = new Date().getFullYear();

        async.eachSeries(underliers, function (item, asyncCallback) {
            db.all(select, [ item.underlier ], function (err, rows) {
                if (err) {
                    closeDb();
                    return asyncCallback(err);
                }

                let total = 0;
                rows.forEach(function (row) {
                    if (row.symbol !== row.underlier) {
                        // Options...
                        let op = common.parseOptionSymbol(row.symbol);
                        if (op.expireDate.getFullYear() === currentYear) {
                            total += row.realizedGain;
                        }
                    }
                    else if (new Date(row.date).getFullYear() === currentYear) {
                        // Stocks...
                        total += row.realizedGain;
                    }
                });
                item.realizedGain = total / 100;
                return asyncCallback(null);
            });
        }, function (err) {
            if (err) {
                closeDb();
                return callback(err);
            }

            select = 'SELECT * FROM positions WHERE date = ? AND underlier = ?';
            let date = new Date();
            date.setHours(0, 0, 0, 0);

            async.eachSeries(underliers, function (item, innerCallBack) {
                db.all(select, [ date.getTime(), item.underlier ], function (err, rows) {
                    if (err) {
                        closeDb();
                        return innerCallBack(err);
                    }

                    let openAmount = 0;
                    rows.forEach(function (row) {
                        if (row.symbol !== row.underlier) {
                            // We include only the options, not the stocks
                            openAmount += row.amount;
                        }
                    });
                    item.openAmount = openAmount / 100;
                    return innerCallBack(null);
                });
            }, function (err) {
                closeDb();

                let result = [];
                underliers.forEach(function (item) {
                    if ((item.realizedGain !== 0) || (item.openAmount !== 0)) {
                        result.push(item);
                    }
                });
                return callback(err, result);
            });
        });
    });
};

/**
 * Parse an option symbol following the format of '-AVGO160422C155' and returns the components as follows:
 *   {
 *      underlier: 'AVGO',
 *      expireDate: timestamp, e.g. 160322,
 *      optionType: OPTION_TYPE.CALL,
 *      strike: 15500
 *   }
 */
let parseOptionSymbol = function (symbol) {
    let optionRegExp = /^-(\D*)([0-9]{6})([C|P])(\S*)/;
    let regRes = optionRegExp.exec(symbol);

    if (!regRes) {
        return null;
    }

    return {
        underlier: regRes[ 1 ],
        expire: regRes[ 2 ],
        type: regRes[ 3 ],
        strike: common.convertCurrencyToInt(regRes[ 4 ]),
    };
};

/**
 * Return all the open and close positions for the specified underlier.
 */
Fidelity.prototype.getPositionsForUnderlier = function (underlier, callback) {
    let db = openDb();
    let select = 'SELECT * FROM positions WHERE underlier=?';
    select += ' AND (status=' + POSITION_STATUS.OPEN + ' OR status=' + POSITION_STATUS.CLOSED + ')';
    select += ' ORDER BY ID ASC, DATE ASC';

    db.all(select, [ underlier ], function (err, rows) {
        if (err) {
            closeDb();
            return callback(err);
        }

        let result = [];

        // Holds positions that have been open but not yet closed.
        // let openPositions = {};

        rows.forEach(function (row) {
            // Option symbol follows a different format. We need to convert it to the one matching this application.
            let op = parseOptionSymbol(row.symbol);
            if (!op) {
                return;
            }

            row.symbol = underlier + op.expire + op.type + op.strike;
            row.date = parseInt(common.dateToTimestamp(new Date(row.date), '093000'));
            row.expireDate = parseInt(common.dateToTimestamp(new Date(row.expireDate), '160000'));

            let key = row.symbol;
            let last = result.length - 1;

            let found = false;
            for (let i = last; i >= 0; --i) {
                let item = result[ i ];
                if (item.symbol === key) {
                    let lastEv = item.events[ item.events.length - 1 ];
                    if ((lastEv.status === POSITION_STATUS.CLOSED) || (lastEv.quantity !== row.quantity)) {
                        item.events.push(row);
                    }
                    found = true;
                    break;
                }
            }

            if (!found) {
                let entry = {
                    symbol: row.symbol,
                    events: [ row ]
                };
                result.push(entry);
            }

            // @formatter:off
            /*
            if (row.status === POSITION_STATUS.CLOSED) {
                // There must be the preceding open position corresponding to this.
                let found = false;
                for (let i = last; i >= 0; --i) {
                    let item = result[ i ];
                    if (item.symbol === key) {
                        item.events.push(row);
                        delete openPositions[ key ];
                        found = true;
                        break;
                    }
                }

                if (!found) {
                    return callback('A position is closed without opened first: ' + row.symbol);
                }
            }
            else {
                // The same open position shows up every day until the position is closed.
                // We need to pick up the first appearance and ignore all the subsequent
                // ones using the lookup table 'openPositions'. Be aware that a position
                // can be closed partially or the size can be increased. We will determine
                // if the position should be ignored by checking the amount...
                let val = openPositions[ key ];
                if (val) {
                    if (val.amount !== row.amount) {
                        for (let i = last; i >= 0; --i) {
                            let item = result[ i ];
                            if (item.symbol === key) {
                                item.events.push(row);
                                break;
                            }
                        }
                    }
                }
                else {
                    // There is no preceding position. Create a new entry.
                    // This may remain open or may have been closed at a later time.
                    let entry = {
                        symbol: row.symbol,
                        events: [ row ]
                    };
                    result.push(entry);
                }
                openPositions[ key ] = row;
            }
            */
            // @formatter:on
        });

        result.forEach(function (item) {
            if (item.events.length > 0) {
                // If a position is partially closed, multiple events would be created on the same day. We want to make sure among those events
                // assigned to the same day the one corresponding to the remaining position comes after the other(s). Without this, you cannot
                // tell whether the position is still open or not just by looking at the last event.
                item.events.sort(function (a, b) {
                    if (a.date !== b.date) {
                        return a.date > b.date;
                    }
                    else {
                        return a.status < b.status;
                    }
                });

                item.startDate = item.events[ 0 ].date;
                item.endDate = 0;
                let lastEvent = item.events[ item.events.length - 1 ];
                if (lastEvent.status === POSITION_STATUS.CLOSED) {
                    item.endDate = lastEvent.date;
                }
            }
        });

        closeDb();
        return callback(null, result);
    });
};

module.exports = new Fidelity();