'use strict';

// Required modules
let common = require('../lib/common');
let fs = require('fs');
let request = require('request');
let async = require('async');

/**
 * The object to be exported.
 */
let Treasury = function () {
};

let rates = {};

let getRateFromLast5Days = function (date, dataSet) {
    let d = new Date(date);
    d.setHours(0, 0, 0, 0);
    for (let i = 0; i < 5; ++i) {
        let dailyRates = dataSet[ d.getTime() ];
        if (dailyRates) {
            return [ dailyRates[ 1 ], dailyRates[ 2 ], dailyRates[ 3 ] ];
        }
        d.setDate(d.getDate() - 1);
    }
    return null;
};

let getRates_ = function (start, end, callback) {
    let result = {};

    let date = common.timestampToDate(start);
    date.setHours(0, 0, 0, 0);

    let endTime = common.timestampToDate(end);
    endTime.setHours(23, 59, 0, 0);
    endTime = endTime.getTime();

    let year = 0;
    let yearlyData = {};

    async.whilst(function () {
        return date.getTime() <= endTime;
    }, function (asyncCallback) {
        if (year != date.getFullYear()) {
            year = date.getFullYear();
            yearlyData = rates[ year ];
            if (!yearlyData) {
                yearlyData = Treasury.prototype.readFromLocalFile(year);
                rates[ year ] = yearlyData;
            }
        }

        if (!yearlyData) {
            // If year's rates are not locally available, we need to download it from the server.
            Treasury.prototype.updateYieldCurve(year, function (err) {
                // Downloaded now. Let's start again (without moving date forward)...
                return asyncCallback(err);
            });
        }
        else {
            let dailyRates = getRateFromLast5Days(date, yearlyData);
            if (dailyRates) {
                let rate = {
                    _3mo: dailyRates[ 0 ],
                    _6mo: dailyRates[ 1 ],
                    _12mo: dailyRates[ 2 ]
                };

                result[ common.dateToTimestamp(date) ] = rate;
                date.setDate(date.getDate() + 1);

                return async.setImmediate(function () {
                    asyncCallback(null);
                });
            }
            else {
                return asyncCallback('No interest rate found.');
            }
        }
    }, function (err) {
        return callback(err, result);
    });
}

Treasury.prototype.getRates = function (start, end, callback) {
    if (common.isEmpty(rates)) {
        // Let's always get this year's rates updated for the first time this method is called.
        Treasury.prototype.updateYieldCurve(new Date().getFullYear(), function (err) {
            if (err) {
                return callback(err);
            }
            return getRates_(start, end, callback);
        });
    }
    else {
        return getRates_(start, end, callback);
    }
};

Treasury.prototype.readFromLocalFile = function (year) {
    let path = common.getDbRoot(common.DATA_TYPE.RATES, true) + year + '.csv';
    if (!common.fileExists(path)) {
        return null;
    }

    let data = {};

    let lines = fs.readFileSync(path, 'utf8').split('\n');
    lines.forEach(function (line) {
        if (line === '') {
            return;
        }
        let tokens = line.split(',');
        let dateStr = tokens[ 0 ]; // yyyy-mm-dd
        let date = new Date(parseInt(dateStr.substr(0, 4)), parseInt(dateStr.substr(5, 7)) - 1, parseInt(dateStr.substr(8, 10)));
        let dailyRates = [];
        let tokenLen = tokens.length;
        for (let j = 1; j < tokenLen; ++j) {
            dailyRates.push(parseFloat(tokens[ j ]));
        }
        data[ date.getTime() ] = dailyRates;
    });

    return data;
};

/**
 * Update the local yield curve rate data
 */
Treasury.prototype.updateYieldCurve = function (year, callback) {
    let httpConfig = {
        url: 'https://www.quandl.com/api/v3/datasets/USTREASURY/YIELD.json',
        qs: {
            api_key: 'K1xpHoACJv3mzuD_D_tx',
            order: 'desc',
            start_date: year + '-01-01',
            end_date: year + '-12-31',
        }
    };

    request.get(httpConfig, function (err, response, body) {
        if (err) {
            return callback(err);
        }

        let json = '';

        try {
            json = JSON.parse(body);

            let lines = "";
            json.dataset.data.forEach(function (item) {
                let line = '';
                for (let pos = 0; pos < item.length; ++pos) {
                    if (pos !== 0) {
                        line += ',';
                    }
                    line += item[ pos ];
                }
                lines += line + '\n';
            });

            let path = common.getDbRoot(common.DATA_TYPE.RATES, true) + year + '.csv';
            fs.writeFileSync(path, lines);
            return callback(null);
        }
        catch (e) {
            console.log(e);
            console.log(json);
            return callback(e);
        }
    });
};

module.exports = new Treasury();