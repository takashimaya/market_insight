'use strict';

// Required modules
let fs = require('fs');
let async = require('async');
let sqlite3 = require('sqlite3');
let http = require('http');
let greeks = require("greeks");

let common = require('../lib/common');
let logger = require('../lib/logger');
let db = require('../lib/db');
let treasury = require('../lib/treasury');

/**
 * The object to be exported.
 */
let Bars = function () {
};

/**
 * Reconstruct and return a Bar object from a line saved in a bar file.
 */
let lineToBar = function (line, useFloat) {
    let tokens = line.split(',');
    let bar = {
        timestamp: parseInt(tokens[ db.BAR_COLUMN.TIMESTAMP ])
    };

    if (useFloat) {
        bar.open = parseFloat(tokens[ db.BAR_COLUMN.OPEN ]);
        bar.high = parseFloat(tokens[ db.BAR_COLUMN.HIGH ]);
        bar.low = parseFloat(tokens[ db.BAR_COLUMN.LOW ]);
        bar.close = parseFloat(tokens[ db.BAR_COLUMN.CLOSE ]);
        bar.size = parseFloat(tokens[ db.BAR_COLUMN.SIZE ]);
    }
    else {
        bar.open = parseInt(tokens[ db.BAR_COLUMN.OPEN ]);
        bar.high = parseInt(tokens[ db.BAR_COLUMN.HIGH ]);
        bar.low = parseInt(tokens[ db.BAR_COLUMN.LOW ]);
        bar.close = parseInt(tokens[ db.BAR_COLUMN.CLOSE ]);
        bar.size = parseInt(tokens[ db.BAR_COLUMN.SIZE ]);
    }

    if (tokens.length > db.BAR_COLUMN.LAST_SIZE) {
        if (useFloat) {
            bar.lastSize = parseFloat(tokens[ db.BAR_COLUMN.LAST_SIZE ]);
            bar.spreadHigh = parseFloat(tokens[ db.BAR_COLUMN.SPREAD_HIGH ]);
            bar.spreadLow = parseFloat(tokens[ db.BAR_COLUMN.SPREAD_LOW ]);
        }
        else {
            bar.lastSize = parseInt(tokens[ db.BAR_COLUMN.LAST_SIZE ]);
            bar.spreadHigh = parseInt(tokens[ db.BAR_COLUMN.SPREAD_HIGH ]);
            bar.spreadLow = parseInt(tokens[ db.BAR_COLUMN.SPREAD_LOW ]);
        }
    }

    return bar;
};

/**
 * Reconstruct and return a Bar object from a line saved in a bar file.
 */
let greekLineToBar = function (line) {
    let tokens = line.split(',');
    let bar = {
        timestamp: parseInt(tokens[ db.BAR_COLUMN.TIMESTAMP ]),
        underlier: parseFloat(tokens[ db.BAR_COLUMN.UNDERLIER ]),
        interestRate: parseFloat(tokens[ db.BAR_COLUMN.INTEREST_RATE ]),
        beta: parseFloat(tokens[ db.BAR_COLUMN.DELTA ]),
        gamma: parseFloat(tokens[ db.BAR_COLUMN.GAMMA ])
    };
    return bar;
};

/**
 * Adjust timestamp to align with the specified 'resolution'.
 */
let adjustBarTimestamp = function (timestamp, resolution, next) {
    let temp = common.timestampToDate(timestamp);
    temp.setSeconds(0, 0);

    if (resolution === common.DATA_TYPE._5MIN) {
        temp.setMinutes(Math.floor(temp.getMinutes() / 5) * 5);
        if (next) {
            temp.setMinutes(temp.getMinutes() + 5);
        }
    }
    else if (resolution === common.DATA_TYPE._30MIN) {
        if (temp.getMinutes() < 30) {
            temp.setMinutes(0);
        }
        else {
            temp.setMinutes(30);
        }

        if (next) {
            temp.setMinutes(temp.getMinutes() + 30);
        }
    }
    else if (resolution === common.DATA_TYPE._1DAY) {
        if ((temp.getHours() < 9) || (temp.getHours() == 6) && (temp.getMinutes() < 30)) {
            if (next) {
                temp.setHours(9, 30, 0, 0);
            }
            else {
                temp.setHours(4, 0, 0, 0);
            }
        }
        else if (temp.getHours() >= 16) {
            if (next) {
                temp.setDate(temp.getDate() + 1);
                temp.setHours(4, 0, 0, 0);
            }
            else {
                temp.setHours(16, 0, 0, 0);
            }
        }
        else {
            if (next) {
                temp.setHours(16, 0, 0, 0);
            }
            else {
                temp.setHours(9, 30, 0, 0);
            }
        }
    }

    return common.dateToTimestamp(temp, null, false);
};

/**
 * Return an initialized bar object with the given timestamp and lastSize set.
 */
let initializeBar = function (timestamp, resolution, lastSize) {
    return {
        timestamp: adjustBarTimestamp(timestamp, resolution),
        open: -1,
        high: -1,
        low: -1,
        close: -1,
        size: 0,
        lastSize: lastSize,
        spreadHigh: -1,
        spreadLow: -1
    };
};

/**
 * Save the bar array into a file, then empty the array and adjust fileStats for the next batch.
 * The bar lookup DB is also updated properly.
 */
let saveBarsAndInitialize = function (bars, symbol, fileStats, resolution, lookupDb) {
    let lines = '';
    let barCount = bars.length;
    for (let i = 0; i < barCount; ++i) {
        let bar = bars[ i ];
        let line = bar.timestamp + ',' + bar.open + ',' + bar.high + ',' + bar.low + ',' + bar.close + ',' + bar.size;
        if (symbol.option) {
            line += ',' + bar.lastSize + ',' + bar.spreadHigh + ',' + bar.spreadLow;
        }
        lines += line + ((i < barCount - 1) ? '\n' : '');
    }

    if (barCount > 0) {
        let start = fileStats.fileStart;
        let id = fileStats.fileId;

        let end = adjustBarTimestamp(bars[ barCount - 1 ].timestamp, resolution, true);
        end = common.timestampToDate(end);
        end.setSeconds(end.getSeconds() - 1);
        end = common.dateToTimestamp(end);

        let path = db.getBarFilePath(symbol.shortSymbol, symbol.underlier, resolution, id, start, true);
        fs.writeFileSync(path, lines);

        // Clone the values because I don't know what will happen when the original values are changed
        // while SQL is executed in the background.
        if (fileStats.creatingNewFile) {
            lookupDb.run(db.INSERT_BAR, [ symbol.shortSymbol, resolution, start, end, id ]);
        }
        else {
            lookupDb.run(db.UPDATE_BAR, [ end, symbol.shortSymbol, resolution, id ]);
        }

        fileStats.fileStart = adjustBarTimestamp(bars[ bars.length - 1 ].timestamp, resolution, true);
        ++fileStats.fileId;
        fileStats.creatingNewFile = true;
        bars.length = 0;
    }
};

let getFuturesCodeStr = function (code, encode) {
    return code[ 0 ] + (encode ? '%20' : '_') + code[ 1 ];
};


/**
 * For each symbol in 'updateBarQueue', we collect all new tick data and update the bar data accordingly.
 */
let updateBarQueue = [];
let isBarUpdating = false;
let lastUnderlierForBarUpdate = undefined;

// Core algorithm. To be called from 'buildBarsFromTicks'.
let buildBarsFromTicks_ = function (symbol, asyncCallback) {
    let fileStats = {
        fileId: undefined,
        fileStart: undefined,
        creatingNewFile: undefined
    };

    // Keeps the current batch of bars (to be saved together in a file).
    let _5minBars = [];
    let _30minBars = [];
    let _1dayBars = [];

    // barPos keeps the start time of the bar currently being updated.
    let barPos;

    // For an option, we accumulate all sizes through its life.
    // We memorize the cummulative size up to the last bar.
    let lastOptionSize = 0;

    let barLookup;
    let tickLookup;
    async.series([ function (asyncCallback2) {
        /////////////////////////////////////////////////////////////////////////////////////////////////
        // Step 1. Get a handle of the bar lookup DB.
        db.getBarLookupDb(function (err, handle) {
            barLookup = handle;
            return asyncCallback2(err);
        });
    }, function (asyncCallback2) {
        /////////////////////////////////////////////////////////////////////////////////////////////////
        // Step 2. If any, load the 5 min bar file that contains the most recent data.
        barLookup.get(db.SELECT_NEWEST_BAR, [ symbol.shortSymbol, common.DATA_TYPE._5MIN ], function (err, row) {
            if (row) {
                let path = db.getBarFilePath(symbol.shortSymbol, symbol.underlier, common.DATA_TYPE._5MIN, row.fileId, row.start, false);
                let lines = fs.readFileSync(path, 'utf8').split('\n');
                lines.forEach(function (line) {
                    _5minBars.push(lineToBar(line, symbol.futures));
                });
                fileStats.fileId = row.fileId;
                fileStats.fileStart = row.start;
                fileStats.creatingNewFile = false;
            }
            return asyncCallback2(err);
        });
    }, function (asyncCallback2) {
        /////////////////////////////////////////////////////////////////////////////////////////////////
        // Step 3. Get a handle of the ticker lookup DB.
        db.getTickerLookupDb(function (err, handle) {
            tickLookup = handle;
            return asyncCallback2(err);
        });
    }, function (asyncCallback2) {
        /////////////////////////////////////////////////////////////////////////////////////////////////
        // Step 4. Determine where to start updating 5 min bars.
        if (_5minBars.length > 0) {
            // There are some bars for the target, and we have already loaded from a file the most recent batch.
            // The last bar is where we should start updating, as the last bar may or may not be complete.
            let bar = _5minBars.pop(); // Get the last bar and remove it from the array.
            barPos = bar.timestamp;
            if (symbol.option) {
                lastOptionSize = bar.lastSize;
            }
            return async.setImmediate(function () {
                asyncCallback2(null);
            });
        }
        else {
            if (symbol.futures) {
                barPos = 20161201000000;
                fileStats.fileStart = barPos;
                fileStats.fileId = 0;
                fileStats.creatingNewFile = true;
                return async.setImmediate(function () {
                    asyncCallback2(null);
                });
            }

            // No bar exists yet. Let's find the oldest tick and we start from there.
            tickLookup.get(db.SELECT_OLDEST_TICK, [ symbol.shortSymbol ], function (err, row) {
                if (row) {
                    barPos = adjustBarTimestamp(row.start, common.DATA_TYPE._5MIN);
                    fileStats.fileStart = barPos;
                    fileStats.fileId = 0;
                    fileStats.creatingNewFile = true;
                }
                return asyncCallback2(err);
            });
        }
    }, function (asyncCallback2) {
        /////////////////////////////////////////////////////////////////////////////////////////////////
        // Step 5. Update 5 min bar data
        if (fileStats.fileId === undefined) {
            return asyncCallback2('No FileId is specified for ' + symbol.shortSymbol + ' [5 min]');
        }

        barPos = parseInt(barPos);
        let bar = initializeBar(barPos, common.DATA_TYPE._5MIN, lastOptionSize);

        if (symbol.futures) {
            let start = common.timestampToDate(barPos);
            let end = new Date();
            end = common.timestampToDate(adjustBarTimestamp(common.dateToTimestamp(end), common.DATA_TYPE._1MIN, true));
            let period = Math.ceil((end.getTime() - start.getTime()) / 1000 / 60); // in minutes
            let config = {
                hostname: 'localhost',
                port: 8002,
                path: '/timeseries?symbols=' + symbol.urlSymbol + '&interval=1&format=json&period=' + period,
                method: 'GET'
            };

            http.get(config, function (response) {
                let body = '';
                response.on('data', function (data) {
                    body += data;
                });
                response.on('end', function () {
                    let array = [];
                    try {
                        let json = JSON.parse(body);
                        array = json[ 0 ].timeSeries;
                    }
                    catch (exp) {
                        logger.add(logger.TYPE.ERROR, 'Bars', 'Error getting future bars: ' + symbol.urlSymbol + ' ' + period);
                        return asyncCallback2(new Error('Server request error.'));
                    }

                    let arrayLen = array.length;
                    let timestamp = 0;
                    // Note the data is sorted from new to old...
                    for (let j = arrayLen - 1; j >= 0; --j) {
                        let rawBar = array[ j ];
                        let d = new Date(rawBar.date);
                        d.setHours(d.getHours() + 3); // Convert from PST/PDT to EST/EDT...
                        timestamp = common.dateToTimestamp(d);

                        if (barPos > timestamp) {
                            continue;
                        }

                        if (symbol.shortSymbol.indexOf('JPY') >= 0) {
                            rawBar.open = 1 / rawBar.open;
                            rawBar.high = 1 / rawBar.high;
                            rawBar.low = 1 / rawBar.low;
                            rawBar.close = 1 / rawBar.close;
                        }

                        if (barPos + 500 /* 5min */ <= timestamp) {
                            // The next tick is outside of the period covered by the current bar.
                            // Finalize the bar and move on to the next.
                            if (bar.open >= 0) { // The current bar in focus contains valid data...
                                _5minBars.push(bar);
                                lastOptionSize += bar.size;
                            }
                            if (_5minBars.length > db.BAR_FILE_MAX_LINES) {
                                // Now we have enough bars at hand. Flush them to a file and prepare for the next batch.
                                saveBarsAndInitialize(_5minBars, symbol, fileStats, common.DATA_TYPE._5MIN, barLookup);
                            }
                            bar = initializeBar(timestamp, common.DATA_TYPE._5MIN, lastOptionSize);
                            barPos = parseInt(bar.timestamp);
                        }

                        bar.size += parseInt(rawBar.volume);

                        if (bar.open === -1) {
                            bar.open = rawBar.open; //common.convertCurrencyToInt();
                        }
                        if ((bar.high === -1) || (bar.high < rawBar.high)) {
                            bar.high = rawBar.high;
                        }
                        if ((bar.low === -1) || (rawBar.low < bar.low)) {
                            bar.low = rawBar.low;
                        }

                        bar.close = rawBar.close;
                    }

                    if ((timestamp >= barPos) && (_5minBars.length > 0)) {
                        saveBarsAndInitialize(_5minBars, symbol, fileStats, common.DATA_TYPE._5MIN, barLookup);
                    }

                    return asyncCallback2(null);
                });
            }).on('error', function (err) {
                return asyncCallback2(err);
            });

            return; // For future, we don't follow the normal procedure. So we exit here...
        }

        // Get all relevant ticks...
        tickLookup.all(db.SELECT_TICKS, [ symbol.shortSymbol, barPos ], function (err, rows) {
            if (rows) {
                // Iterate all tick files...
                for (let i = 0; i < rows.length; ++i) {
                    let row = rows[ i ];
                    let path = db.getTickFilePath(symbol.shortSymbol, symbol.underlier, row.fileId, row.start, false);
                    let ticks = fs.readFileSync(path, 'utf8').split('\n');

                    // Load the contents (i.e. the array of contiguous ticks) of each file...
                    let tickLen = ticks.length;
                    for (let j = 0; j < tickLen; ++j) {
                        let tick = ticks[ j ];
                        let tokens = tick.split(',');

                        // Tick's timstamp is in millisec. Divide it by 1000 to be in sec.
                        let tickTimestamp = Math.round(parseInt(tokens[ db.TICK_COLUMN.TIMESTAMP ]) / 1000);

                        if (!symbol.option && (tokens[ db.TICK_COLUMN.TYPE ] === 'Q')) {
                            // We ignore Q's (quotes) for underliers...
                            continue;
                        }

                        if (barPos > tickTimestamp) {
                            continue;
                        }

                        if (barPos + 500 /* 5min */ <= tickTimestamp) {
                            // The next tick is outside of the period covered by the current bar.
                            // Finalize the bar and move on to the next.
                            if (bar.open >= 0) { // The current bar in focus contains valid data...
                                _5minBars.push(bar);
                                lastOptionSize += bar.size;
                            }
                            if (_5minBars.length > db.BAR_FILE_MAX_LINES) {
                                // Now we have enough bars at hand. Flush them to a file and prepare for the next batch.
                                saveBarsAndInitialize(_5minBars, symbol, fileStats, common.DATA_TYPE._5MIN, barLookup);
                            }
                            bar = initializeBar(tickTimestamp, common.DATA_TYPE._5MIN, lastOptionSize);
                            barPos = parseInt(bar.timestamp);
                        }

                        if (symbol.option) {
                            if (tokens[ db.TICK_COLUMN.TYPE ] === 'Q') {
                                let bid = common.convertCurrencyToInt(tokens[ db.TICK_COLUMN.Q_BID ]);
                                let ask = common.convertCurrencyToInt(tokens[ db.TICK_COLUMN.Q_ASK ]);
                                let price = Math.round((bid + ask) / 2);
                                let spread = (ask - bid);

                                if (bar.open === -1) {
                                    bar.open = price;
                                }
                                if ((bar.high === -1) || (bar.high < price)) {
                                    bar.high = price;
                                }
                                if ((bar.low === -1) || (price < bar.low)) {
                                    bar.low = price;
                                }
                                bar.close = price;

                                if ((bar.spreadHigh === -1) || (bar.spreadHigh < spread)) {
                                    bar.spreadHigh = spread;
                                }
                                if ((bar.spreadLow === -1) || (spread < bar.spreadLow)) {
                                    bar.spreadLow = spread;
                                }
                            }
                            else {
                                bar.size += parseInt(tokens[ db.TICK_COLUMN.T_SIZE ]);
                            }
                        }
                        else {
                            // This is underlier. We only pick up 'T's (trade) and ignore 'Q's (quotes).
                            if (tokens[ db.TICK_COLUMN.TYPE ] === 'T') {
                                bar.size += parseInt(tokens[ db.TICK_COLUMN.T_SIZE ]);

                                let price = common.convertCurrencyToInt(tokens[ db.TICK_COLUMN.T_PRICE ]);

                                if (bar.open === -1) {
                                    bar.open = price;
                                }
                                if ((bar.high === -1) || (bar.high < price)) {
                                    bar.high = price;
                                }
                                if ((bar.low === -1) || (price < bar.low)) {
                                    bar.low = price;
                                }

                                bar.close = price;
                            }
                        }
                    }
                }

                if (_5minBars.length > 0) {
                    saveBarsAndInitialize(_5minBars, symbol, fileStats, common.DATA_TYPE._5MIN, barLookup);
                }
            }
            return asyncCallback2(err);
        });
    }, function (asyncCallback2) {
        // Reset variables...
        barPos = undefined;
        lastOptionSize = 0;
        fileStats = {
            fileId: undefined,
            fileStart: undefined,
            creatingNewFile: undefined
        };

        /////////////////////////////////////////////////////////////////////////////////////////////////
        // Step 6. If any, load the 30 min bar file that contains the most recent data.
        barLookup.get(db.SELECT_NEWEST_BAR, [ symbol.shortSymbol, common.DATA_TYPE._30MIN ], function (err, row) {
            if (row) {
                let path = db.getBarFilePath(symbol.shortSymbol, symbol.underlier, common.DATA_TYPE._30MIN, row.fileId, row.start, false);
                let lines = fs.readFileSync(path, 'utf8').split('\n');
                lines.forEach(function (line) {
                    _30minBars.push(lineToBar(line, symbol.futures));
                });
                fileStats.fileId = row.fileId;
                fileStats.fileStart = row.start;
                fileStats.creatingNewFile = false;
            }
            return asyncCallback2(err);
        });
    }, function (asyncCallback2) {
        /////////////////////////////////////////////////////////////////////////////////////////////////
        // Step 7. Determine where to start updating 30 min bars.
        if (_30minBars.length > 0) {
            // There are some bars for the target, and we have already loaded from a file the most recent batch.
            // The last bar is where we should start updating, as the last bar may or may not be complete.
            let bar = _30minBars.pop(); // Get the last bar and remove it from the array.
            barPos = bar.timestamp;
            if (symbol.option) {
                lastOptionSize = bar.lastSize;
            }
            return asyncCallback2(null);
        }
        else {
            // No bar exists yet. Let's find the oldest 5 min data and we start from there.
            barLookup.get(db.SELECT_OLDEST_BAR, [ symbol.shortSymbol, common.DATA_TYPE._5MIN ], function (err, row) {
                if (row) {
                    barPos = adjustBarTimestamp(row.start, common.DATA_TYPE._30MIN);
                    fileStats.fileStart = barPos;
                    fileStats.fileId = 0;
                    fileStats.creatingNewFile = true;
                }
                return asyncCallback2(err);
            });
        }
    }, function (asyncCallback2) {
        /////////////////////////////////////////////////////////////////////////////////////////////////
        // Step 8. Update 30 min bar data
        if (fileStats.fileId === undefined) {
            return asyncCallback2('No FileId is specified for ' + symbol.shortSymbol + ' [30 min]');
        }

        barPos = parseInt(barPos);
        let bar = initializeBar(barPos, common.DATA_TYPE._30MIN, lastOptionSize);

        // Get all relevant 5 min bars...
        barLookup.all(db.SELECT_BARS, [ symbol.shortSymbol, common.DATA_TYPE._5MIN, barPos, '20991231000000' ], function (err, rows) {
            if (rows) {
                // Iterate all 5 min bar files...
                for (let i = 0; i < rows.length; ++i) {
                    let row = rows[ i ];
                    let path = db.getBarFilePath(symbol.shortSymbol, symbol.underlier, common.DATA_TYPE._5MIN, row.fileId, row.start, false);
                    let lines = fs.readFileSync(path, 'utf8').split('\n');

                    // Load the contents (i.e. the array of contiguous 5 min bars) of each file...
                    let len = lines.length;
                    for (let j = 0; j < len; ++j) {
                        let line = lines[ j ];
                        let tokens = line.split(',');

                        let timestamp = parseInt(tokens[ db.BAR_COLUMN.TIMESTAMP ]);

                        if (barPos > timestamp) {
                            continue;
                        }

                        if (barPos + 3000 /* 30min */ <= timestamp) {
                            // The next tick is outside of the period covered by the current bar.
                            // Finalize the bar and move on to the next.
                            if (bar.open >= 0) { // The current bar in focus contains valid data...
                                _30minBars.push(bar);
                                lastOptionSize += bar.size;
                            }
                            if (_30minBars.length > db.BAR_FILE_MAX_LINES) {
                                // Now we have enough bars at hand. Flush them to a file and prepare for the next batch.
                                saveBarsAndInitialize(_30minBars, symbol, fileStats, common.DATA_TYPE._30MIN, barLookup);
                            }
                            bar = initializeBar(timestamp, common.DATA_TYPE._30MIN, lastOptionSize);
                            barPos = parseInt(bar.timestamp);
                        }

                        bar.size += parseInt(tokens[ db.BAR_COLUMN.SIZE ]);

                        if (bar.open === -1) {
                            bar.open = symbol.futures ? parseFloat(tokens[ db.BAR_COLUMN.OPEN ]) : parseInt(tokens[ db.BAR_COLUMN.OPEN ]);
                        }

                        let high = symbol.futures ? parseFloat(tokens[ db.BAR_COLUMN.HIGH ]) : parseInt(tokens[ db.BAR_COLUMN.HIGH ]);
                        if ((bar.high === -1) || (bar.high < high)) {
                            bar.high = high;
                        }

                        let low = symbol.futures ? parseFloat(tokens[ db.BAR_COLUMN.LOW ]) : parseInt(tokens[ db.BAR_COLUMN.LOW ]);
                        if ((bar.low === -1) || (low < bar.low)) {
                            bar.low = low;
                        }

                        bar.close = symbol.futures ? parseFloat(tokens[ db.BAR_COLUMN.CLOSE ]) : parseInt(tokens[ db.BAR_COLUMN.CLOSE ]);

                        if (symbol.option) {
                            let spreadHigh = symbol.futures ? parseFloat(tokens[ db.BAR_COLUMN.SPREAD_HIGH ]) : parseInt(tokens[ db.BAR_COLUMN.SPREAD_HIGH ]);
                            if ((bar.spreadHigh === -1) || (bar.spreadHigh < spreadHigh)) {
                                bar.spreadHigh = spreadHigh;
                            }

                            let spreadLow = symbol.futures ? parseFloat(tokens[ db.BAR_COLUMN.SPREAD_LOW ]) : parseInt(tokens[ db.BAR_COLUMN.SPREAD_LOW ]);
                            if ((bar.spreadLow === -1) || (spreadLow < bar.spreadLow)) {
                                bar.spreadLow = spreadLow;
                            }
                        }
                    }
                }

                if (_30minBars.length > 0) {
                    saveBarsAndInitialize(_30minBars, symbol, fileStats, common.DATA_TYPE._30MIN, barLookup);
                }
            }
            return asyncCallback2(err);
        });
    }, function (asyncCallback2) {
        if (symbol.option) {
            // We won't create 1 day bar for options...
            return asyncCallback2(null);
        }

        // Reset variables...
        barPos = undefined;
        lastOptionSize = 0;
        fileStats = {
            fileId: undefined,
            fileStart: undefined,
            creatingNewFile: undefined
        };

        /////////////////////////////////////////////////////////////////////////////////////////////////
        // Step 9. If any, load the 1 day bar file that contains the most recent data.
        barLookup.get(db.SELECT_NEWEST_BAR, [ symbol.shortSymbol, common.DATA_TYPE._1DAY ], function (err, row) {
            if (row) {
                let path = db.getBarFilePath(symbol.shortSymbol, symbol.underlier, common.DATA_TYPE._1DAY, row.fileId, row.start, false);
                let lines = fs.readFileSync(path, 'utf8').split('\n');
                lines.forEach(function (line) {
                    _1dayBars.push(lineToBar(line, symbol.futures));
                });
                fileStats.fileId = row.fileId;
                fileStats.fileStart = row.start;
                fileStats.creatingNewFile = false;
            }
            return asyncCallback2(err);
        });
    }, function (asyncCallback2) {
        if (symbol.option) {
            // We won't create 1 day bar for options...
            return asyncCallback2(null);
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        // Step 10. Determine where to start updating 1 day bars.
        if (_1dayBars.length > 0) {
            // There are some bars for the target, and we have already loaded from a file the most recent batch.
            // The last bar is where we should start updating, as the last bar may or may not be complete.
            let bar = _1dayBars.pop(); // Get the last bar and remove it from the array.
            barPos = bar.timestamp;
            if (symbol.option) {
                lastOptionSize = bar.lastSize;
            }
            return asyncCallback2(null);
        }
        else {
            // No bar exists yet. Let's find the oldest 30 min data and we start from there.
            barLookup.get(db.SELECT_OLDEST_BAR, [ symbol.shortSymbol, common.DATA_TYPE._30MIN ], function (err, row) {
                if (row) {
                    barPos = adjustBarTimestamp(row.start, common.DATA_TYPE._1DAY);
                    fileStats.fileStart = barPos;
                    fileStats.fileId = 0;
                    fileStats.creatingNewFile = true;
                }
                return asyncCallback2(err);
            });
        }
    }, function (asyncCallback2) {
        if (symbol.option) {
            // We won't create 1 day bar for options...
            return asyncCallback2(null);
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        // Step 11. Update 1 day bar data
        if (fileStats.fileId === undefined) {
            return asyncCallback2('No FileId is specified for ' + symbol.shortSymbol + ' [1 day]');
        }

        barPos = parseInt(barPos);
        let bar = initializeBar(barPos, common.DATA_TYPE._1DAY, lastOptionSize);
        let barPosHour = common.timestampToDate(barPos).getHours();

        // Get all relevant 30 min bars...
        barLookup.all(db.SELECT_BARS, [ symbol.shortSymbol, common.DATA_TYPE._30MIN, barPos, '20991231000000' ], function (err, rows) {
            if (rows) {
                // Iterate all 30 min bar files...
                for (let i = 0; i < rows.length; ++i) {
                    let row = rows[ i ];
                    let path = db.getBarFilePath(symbol.shortSymbol, symbol.underlier, common.DATA_TYPE._30MIN, row.fileId, row.start, false);
                    let lines = fs.readFileSync(path, 'utf8').split('\n');

                    // Load the contents (i.e. the array of contiguous 30 min bars) of each file...
                    let len = lines.length;
                    for (let j = 0; j < len; ++j) {
                        let line = lines[ j ];
                        let tokens = line.split(',');

                        let timestamp = parseInt(tokens[ db.BAR_COLUMN.TIMESTAMP ]);

                        if (barPos > timestamp) {
                            continue;
                        }

                        // For 1 day bar, we'll have 3 bars for a day. One for before market (starting at 4 am), one for market hours (starting at 9:30 am),
                        // and one for after market (starting at 4 pm)
                        let hours = 0;
                        let minutes = 0;
                        if (barPosHour === 4) {
                            // From 4 am to 9:30 am --> 5.5 hours
                            hours = 50000;
                            minutes = 3000;
                        }
                        else if (barPosHour === 9) {
                            // From 9:30 am to 4:00 pm --> 7.5 hours
                            hours = 70000;
                            minutes = -3000;
                        }
                        else {
                            // From 4:00 pm to 8:00 pm --> 4 hours
                            hours = 40000;
                            minutes = 1; // There is some trade right on 8:00 pm. Let's have a margin...
                        }

                        if (barPos + hours + minutes <= timestamp) {
                            // The next tick is outside of the period covered by the current bar.
                            // Finalize the bar and move on to the next.
                            if (bar.open >= 0) { // The current bar in focus contains valid data...
                                _1dayBars.push(bar);
                                lastOptionSize += bar.size;
                            }
                            if (_1dayBars.length > db.BAR_FILE_MAX_LINES) {
                                // Now we have enough bars at hand. Flush them to a file and prepare for the next batch.
                                saveBarsAndInitialize(_1dayBars, symbol, fileStats, common.DATA_TYPE._1DAY, barLookup);
                            }
                            bar = initializeBar(timestamp, common.DATA_TYPE._1DAY, lastOptionSize);
                            barPos = parseInt(bar.timestamp);
                            barPosHour = common.timestampToDate(barPos).getHours();
                        }

                        bar.size += parseInt(tokens[ db.BAR_COLUMN.SIZE ]);

                        if (bar.open === -1) {
                            bar.open = symbol.futures ? parseFloat(tokens[ db.BAR_COLUMN.OPEN ]) : parseInt(tokens[ db.BAR_COLUMN.OPEN ]);
                        }

                        let high = symbol.futures ? parseFloat(tokens[ db.BAR_COLUMN.HIGH ]) : parseInt(tokens[ db.BAR_COLUMN.HIGH ]);
                        if ((bar.high === -1) || (bar.high < high)) {
                            bar.high = high;
                        }

                        let low = symbol.futures ? parseFloat(tokens[ db.BAR_COLUMN.LOW ]) : parseInt(tokens[ db.BAR_COLUMN.LOW ]);
                        if ((bar.low === -1) || (low < bar.low)) {
                            bar.low = low;
                        }

                        bar.close = symbol.futures ? parseFloat(tokens[ db.BAR_COLUMN.CLOSE ]) : parseInt(tokens[ db.BAR_COLUMN.CLOSE ]);

                        if (symbol.option) {
                            let spreadHigh = symbol.futures ? parseFloat(tokens[ db.BAR_COLUMN.SPREAD_HIGH ]) : parseInt(tokens[ db.BAR_COLUMN.SPREAD_HIGH ]);
                            if ((bar.spreadHigh === -1) || (bar.spreadHigh < spreadHigh)) {
                                bar.spreadHigh = spreadHigh;
                            }

                            let spreadLow = symbol.futures ? parseFloat(tokens[ db.BAR_COLUMN.SPREAD_LOW ]) : parseInt(tokens[ db.BAR_COLUMN.SPREAD_LOW ]);
                            if ((bar.spreadLow === -1) || (spreadLow < bar.spreadLow)) {
                                bar.spreadLow = spreadLow;
                            }
                        }
                    }
                }

                if (_1dayBars.length > 0) {
                    saveBarsAndInitialize(_1dayBars, symbol, fileStats, common.DATA_TYPE._1DAY, barLookup);
                }
            }
            return asyncCallback2(err);
        });
    } ], function (err) {
        if (err) {
            if (err.toString().indexOf('No FileId is specified for') !== -1) {
                // console.log('ERROR! Bar data update failed: "' + err + '"');

                // Unfortunately, ActiveTicks returns no data for some strike/expire, which TradeKing claims exists.
                // There is really no quotes or trades for it?? Anyway, for those, we don't get a tick data and get this error.
                // For now, we'll just ignore it...
                return asyncCallback(null);
            }
        }
        return asyncCallback(err);
    });
};

let barUpdateCount = 0;
let barUpdateTime = 0;
let lastBarUpdateStart;
let buildBarsFromTicks = function () {
    if (isBarUpdating) {
        // The process is already running.
        return;
    }
    isBarUpdating = true;

    async.whilst(function () {
        return updateBarQueue.length > 0;
    }, function (asyncCallback) {
        // Get the next target from FIFO queue.
        let symbol = updateBarQueue.shift();
        let updateDb;
        db.getBarUpdateDb(function (err, handle) {
            if (err) {
                asyncCallback(err);
            }

            updateDb = handle;
            updateDb.get(db.SELECT_BAR_UPDATE, [ symbol.shortSymbol ], function (err, row) {
                if (err) {
                    asyncCallback(err);
                }

                symbol.entryExistsInUpdateDb = (row !== undefined);

                if (row) {
                    if (!common.moreDataSinceLastUpdate(new Date(row.lastUpdate), symbol.option, symbol.futures)) {
                        return asyncCallback(null);
                    }
                }

                ++barUpdateCount;
                if (lastBarUpdateStart) {
                    barUpdateTime += (new Date().getTime() - lastBarUpdateStart.getTime()) / 1000;
                }
                lastBarUpdateStart = new Date();
                let underlier = symbol.futures ? 'futures' : symbol.underlier;
                if (!lastUnderlierForBarUpdate || (lastUnderlierForBarUpdate !== underlier) || (barUpdateCount % 500 === 0)) {
                    let now = new Date();
                    let msg = symbol.shortSymbol + ' [' + updateBarQueue.length + '][' + barUpdateCount + '] ';
                    msg += Math.round(barUpdateTime) + ' sec. @' + now.toLocaleTimeString();
                    logger.add(logger.TYPE.UPDATE, 'Bars', msg);
                    lastUnderlierForBarUpdate = underlier;
                }

                // There is no guarantee that the ticks up to this moment are already available.
                // For instance, it is possible that a symbol have been sitting in the queue
                // for 30 minutes in which case ticks are available only up to 30 minutes ago.
                // So we changed the algorithm - lastUpdate info is set by the caller (e.g.
                // updateTicks), who knows about the most recent data.

                // symbol.lastUpdate = new Date().getTime();

                buildBarsFromTicks_(symbol, function (err) {
                    if (err) {
                        return asyncCallback(err);
                    }

                    if (symbol.entryExistsInUpdateDb) {
                        updateDb.run(db.UPDATE_BAR_UPDATE, [ symbol.lastUpdate, symbol.shortSymbol ], function (err) {
                            return asyncCallback(err);
                        });
                    }
                    else {
                        updateDb.run(db.INSERT_BAR_UPDATE, [ symbol.shortSymbol, symbol.lastUpdate ], function (err) {
                            return asyncCallback(err);
                        });
                    }
                });
            });
        });
    }, function (err) {
        if (err) {
            logger.add(logger.TYPE.ERROR, 'Bar', err)

        }
        isBarUpdating = false;
    });
};

Bars.prototype.slaveUpdateBars = function () {
    let path = __dirname + common.getPathSeparator() + 'targets.csv';
    let lines = fs.readFileSync(path, 'utf8').split('\r\n');

    async.eachSeries(lines, function (line, asyncCallback2) {
        if (line === '') {
            return async.setImmediate(function () {
                asyncCallback2(null);
            });
        }

        let tokens = line.split(',');

        let symbol = tokens[ 0 ];
        let active = parseInt(tokens[ 1 ]);

        if (active === 0) {
            return async.setImmediate(function () {
                asyncCallback2(null);
            });
        }

        // Update Frequency:
        //    0 ... update during market close
        //    1 ... real time update
        //    2 ... real time update (underlier only. options will be updated during market close)
        let updateFrequency = (tokens.length > 2) ? parseInt(tokens[ 2 ]) : 0;

        // Options:
        //    0 ... no option chain
        //    1 ... full option chain (both weekly and monthly)
        //    2 ... monthly only
        let optionMode = (tokens.length > 3) ? parseInt(tokens[ 3 ]) : 0;

        let otmRange = (tokens.length > 4) ? parseFloat(tokens[ 4 ]) : 0.15;
        let itmRange = (tokens.length > 5) ? parseFloat(tokens[ 5 ]) : 0.1;

        let target = {
            symbol: symbol,
            option: false,
            underlier: symbol,
            shortSymbol: symbol
        };

    });
};

/**
 * Update bar data based on new tick data.
 */
Bars.prototype.updateBars = function (symbol) {
    // Add the new symbol to the queue and invoke the background task to process the queue.
    let len = updateBarQueue.length;
    for (let i = 0; i < len; ++i) {
        if (updateBarQueue[ i ].shortSymbol === symbol.shortSymbol) {
            // The symbol is already in the queue.
            updateBarQueue[ i ].lastUpdate = symbol.lastUpdate;
            return;
        }
    }

    if (symbol.futures) {
        // Let's give futures a priority. Put it on top of the queue instead of the bottom.
        updateBarQueue.unshift(symbol);
    }
    else {
        updateBarQueue.push(symbol);
    }
    return setTimeout(function () {
        buildBarsFromTicks();
    }, 100);
};

/**
 * Build bars for the futures (and anything else) whose data come from InteractiveBrokers
 * not from ActiveTicks.
 */
let firstFutureBarUpdate = true;
Bars.prototype.updateBarsForFutures = function () {
    // We define the symbol sets here...
    // @formatter:off
    let codes = [ ['CL', 1],
                  ['CL', 6],
                  ['CL', 12],
                  ['BZ', 2],
                  ['GC', 2],
                  ['GC', 4],
                  ['GC', 6],
                  ['SI', 0],
                  ['HG', 0],
                  ['JPY', 0],
                  ['EUR', 0],
                  ['CHF', 0],
                  ['GBP', 0],
                  ['CAD', 0],
                  ['AUD', 0],
                  ['NZD', 0],
                  ['GE', 1],
                  ['GE', 2],
                  ['GE', 3],
                  ['GE', 6],
                  ['GE', 9],
                  ['GE', 12],
                  ['GE', 24],
                  ['GE', 36],
                  ['GE', 48],
                  ['GE', 60],
                  ['GE', 72],
                  ['ZT', 1],
                  ['ZF', 1],
                  ['ZN', 1]];
    // @formatter:on

    if (firstFutureBarUpdate) {
        // Futures bars have a unique problem. As we get the data through intermediary (IBAccess), which doesn't guarantee that no bar is missing
        // up to the minute, we are not certain if the bars acquired so far are packed (i.e. no missing data). There would be a smart way to handle
        // it, but we do it in a dumb way --- we erase the data back to (at least) a month ago and reload them all again! When IB access relaunches,
        // it will fill in missing data if any, so as long as this happens at least once a month, we'll be ok.

        firstFutureBarUpdate = false;

        let amonthAgo = new Date();
        amonthAgo.setMonth(amonthAgo.getMonth() - 1);
        amonthAgo = common.dateToTimestamp(amonthAgo);

        let symbol;
        let underlier;
        async.eachSeries(codes, function (code, asyncCallback) {
            async.series([ function (asyncCallback2) {
                symbol = getFuturesCodeStr(code);
                underlier = code[ 0 ];

                db.getBarUpdateDb(function (err, handle) {
                    if (err) {
                        return asyncCallback2(err);
                    }

                    handle.get(db.SELECT_BAR_UPDATE, [ symbol ], function (err, row) {
                        if (err) {
                            return asyncCallback2(err);
                        }

                        if (row) {
                            // Reset update date in order to enforce update in 'buildBarsFromTicks'.
                            handle.run(db.UPDATE_BAR_UPDATE, [ new Date(2000, 0, 1).getTime(), symbol ], function (err) {
                                return asyncCallback2(err);
                            });
                        }
                        else {
                            return asyncCallback2(null);
                        }
                    });
                });
            }, function (asyncCallback2) {
                db.getBarLookupDb(function (err, handle) {
                    if (err) {
                        return asyncCallback2(err);
                    }

                    handle.all(db.SELECT_BARS_WITH_ALL_RESOLUTIONS, [ symbol, amonthAgo ], function (err, rows) {
                        if (err) {
                            return asyncCallback2(err);
                        }

                        if (!rows) {
                            return asyncCallback2(null);
                        }

                        async.eachSeries(rows, function (row, asyncCallback3) {
                            handle.run(db.DELETE_BAR, [ symbol, row.resolution, row.fileId ], function (err) {
                                if (err) {
                                    return asyncCallback3(err);
                                }
                                let path = db.getBarFilePath(symbol, underlier, row.resolution, row.fileId, row.start, false);
                                if (common.fileExists(path)) {
                                    fs.unlinkSync(path);
                                }
                                return asyncCallback3(null);
                            });
                        }, function (err) {
                            return asyncCallback2(err);
                        });
                    });
                });
            } ], function (err) {
                return asyncCallback(err);
            });
        }, function (err) {
            setTimeout(function () {
                Bars.prototype.updateBarsForFutures();
            }, 1000);
        });
    }

    let len = codes.length;
    for (let i = len - 1; i >= 0; --i) {
        let code = codes[ i ];
        let symbol = getFuturesCodeStr(code);
        let futures = {
            symbol: symbol,
            option: false,
            underlier: code[ 0 ],
            shortSymbol: symbol,
            urlSymbol: getFuturesCodeStr(code, true),
            futures: true,
            lastUpdate: new Date(2000, 0, 1) // Always force update
        };
        Bars.prototype.updateBars(futures);
    }

    logger.add(logger.TYPE.INFO, 'Futures', 'Bar update queued.');
    setTimeout(function () {
        Bars.prototype.updateBarsForFutures();
    }, 600000); // Invoke every 10 minutes...
};

Bars.prototype.getBars = function (symbols, start, end, resolution, callback) {
    let result = {};
    result.bars = {};

    let tradeHours = {};
    let startDate = undefined;
    let endDate = undefined;

    let lookupDb;

    async.series([ function (asyncCallback) {
        /////////////////////////////////////////////////////////////////////////////////////////////////
        // Step 1. Get a handle of the bar lookup DB.
        db.getBarLookupDb(function (err, handle) {
            lookupDb = handle;
            return async.setImmediate(function () {
                asyncCallback(null);
            });
        });
    }, function (asyncCallback) {
        /////////////////////////////////////////////////////////////////////////////////////////////////
        // Step 2. Iterate symbols and get bars for each.
        async.eachSeries(symbols, function (symbol, asyncCallback2) {
            let option = common.parseOptionSymbol(symbol);
            let bars = [];
            result.bars[ symbol ] = bars;
            lookupDb.all(db.SELECT_BARS, [ symbol, resolution, start, end ], function (err, rows) {
                if (!rows) {
                    return asyncCallback2(err);
                }

                let futures = false;
                async.eachSeries(rows, function (row, asyncCallback3) {
                    let underlier = symbol;
                    // Futures has the following formats: CL_1 - CL (Texas Sweet) 1 (Front Month)
                    if (symbol.indexOf('_') > 0) {
                        underlier = symbol.split('_')[ 0 ];
                        futures = true;
                    }
                    if (option) {
                        underlier = option.underlier;
                    }
                    let path = db.getBarFilePath(symbol, underlier, resolution, row.fileId, row.start, false);
                    let lines = fs.readFileSync(path, 'utf8').split('\n');
                    let lineGreeks = null;
                    let greekBars = {};

                    async.series([ function (asyncCallback4) {
                        if (!option) {
                            return async.setImmediate(function () {
                                asyncCallback4(null);
                            });
                        }

                        // If the symbol is an option, then there is an associated file for greeks.
                        let pathGreeks = db.getBarFilePath(symbol + '_greeks', underlier, resolution, row.fileId, row.start, false);
                        if (common.fileExists(pathGreeks)) {
                            lineGreeks = fs.readFileSync(pathGreeks, 'utf8').split('\n');
                            if (line.length !== lineGreeks.length) {
                                // If the number of lines doesn't match, some greeks are missing. So, we need to update now.
                                lineGreeks = null;
                            }
                            else {
                                lineGreeks.forEach(function (line) {
                                    let bar = greekLineToBar(line);
                                    greekBars[ bar.timestamp ] = bar;
                                });
                            }
                        }

                        if (lineGreeks) {
                            // OK. We got greeks. Ready to move next.
                            return async.setImmediate(function () {
                                asyncCallback4(null);
                            });
                        }

                        let startBar = lineToBar(lines[ 0 ], futures);
                        let endBar = lineToBar(lines[ lines.length - 1 ], futures);

                        let interestRates = {};

                        // We need to calculate greeks. We need underlier's price and interest rate...
                        async.series([ function (asyncCallback5) {
                            treasury.getRates(startBar.timestamp, endBar.timestamp, function (err, result) {
                                interestRates = result;
                                return asyncCallback5(null);
                            });
                        }, function (asyncCallback5) {
                            // We do a recursive call, but the underlier is not an option so this part will be skipped there.
                            Bars.prototype.getBars([ underlier ], startBar.timestamp, endBar.timestamp, row.resolution, function (err, underlierBars) {
                                if (err) {
                                    return asyncCallback5(err);
                                }

                                let underlierBarLength = underlierBars.length;
                                let underlierBarIndex = 0;
                                let barIndex = 0;
                                lines.forEach(function (line) {
                                    let bar = lineToBar(line, futures);
                                    let underlierBar = null;
                                    let underlierBarTimestamp = 0;
                                    while ((underlierBarTimestamp < bar.timestamp) && (underlierBarIndex < underlierBarLength)) {
                                        underlierBar = underlierBars[ underlierBarIndex++ ];
                                        underlierBarTimestamp = underlierBar.timestamp;
                                    }

                                    // Ideally, we use the underlier's price with the same timestamp as option's.
                                    // If it doesn't exist, we'll use the one immediately afterwards...
                                    let d = common.timestampToDate(underlierBarTimestamp);
                                    d.setHours(0, 0, 0, 0);
                                    let rates = interestRates[ d ];

                                    // Now we got both underlier price and interest rate. Read to calculate greeks.
                                    // greeks.getDelta(underlierBar.close, option.strike, option.expireDate, )
                                });
                                return asyncCallback5(null);
                            });
                        } ], function (err, results) {
                            return asyncCallback4(err);
                        });
                    }, function (asyncCallback4) {
                        lines.forEach(function (line) {
                            let bar = lineToBar(line, futures);
                            if ((bar.timestamp >= start) && (bar.timestamp <= end)) {
                                if (option) {
                                    bar.underlier = 0;
                                    bar.delta = 0;
                                    bar.theta = 0;
                                    bar.gamma = 0;
                                    bar.vega = 0;
                                }

                                bars.push(bar);

                                let d = common.timestampToDate(bar.timestamp);
                                let date = d.getFullYear() * 10000 + (d.getMonth() + 1) * 100 + d.getDate();

                                if (!startDate || (d.getTime() < startDate.getTime())) {
                                    startDate = d;
                                }
                                if (!endDate || endDate.getTime() < d.getTime()) {
                                    endDate = d;
                                }

                                let hours = tradeHours[ date ];
                                if (!hours) {
                                    tradeHours[ date ] = {
                                        extStart: bar.timestamp,
                                        extEnd: bar.timestamp
                                    };
                                }
                                else {
                                    if (bar.timestamp < hours.extStart) {
                                        hours.extStart = bar.timestamp;
                                    }
                                    if (hours.extEnd < bar.timestamp) {
                                        hours.extEnd = bar.timestamp;
                                    }
                                }
                            }
                        });

                        return async.setImmediate(function () {
                            asyncCallback4(null);
                        });
                    } ], function (err) {
                        return asyncCallback3(err);
                    });
                }, function (err) {
                    return asyncCallback2(err);
                });
            });
        }, function (err) {
            return asyncCallback(err);
        });
    } ], function (err) {
        if (err) {
            result.status = err;
        }
        else {
            result.status = 'SUCCESS';

            let tradeHoursArray = [];

            if (startDate) {
                let extSec = 0;
                let stdSec = 0;
                let d = new Date(startDate);
                d.setHours(0, 0, 0, 0);
                while (d.getTime() <= endDate.getTime()) {
                    let key = d.getFullYear() * 10000 + (d.getMonth() + 1) * 100 + d.getDate();
                    let obj = tradeHours[ key ];
                    // Note 'obj' is undefined if the market is closed on 'd' date.
                    if (obj) {
                        obj.extSec = extSec;
                        obj.stdSec = stdSec;
                        tradeHoursArray.push(obj);
                        let a = common.timestampToDate(obj.extEnd).getTime();
                        let b = common.timestampToDate(obj.extStart).getTime();
                        extSec += Math.round((a - b) / 1000);

                        let day = d.getDay();
                        if ((1 <= day) && (day <= 5)) {
                            stdSec += (13 - 6.5) * 60 * 60; // Std market hours: 6:30 to 13:00 PDT/PST
                        }
                    }
                    d.setDate(d.getDate() + 1);
                }
            }

            result.tradeHours = tradeHoursArray;
        }
        return callback(err, result);
    });
};

Bars.prototype.getOptionChain = function (underlier, start, end, resolution, callback) {
    let result = {
        status: 'SUCCESS',
        optionChain: [],
        expires: [],
        strikes: [],
        underlierPrice: 0
    };

    let lookupDb;

    async.series([ function (asyncCallback) {
        /////////////////////////////////////////////////////////////////////////////////////////////////
        // Step 1. Get a handle of the bar lookup DB.
        db.getBarLookupDb(function (err, handle) {
            lookupDb = handle;
            return asyncCallback(err);
        });
    }, function (asyncCallback) {
        lookupDb.all(db.SELECT_DISTINCT_UNDERLIERS, [ '%' + underlier + '%', resolution, start, end ], function (err, rows) {
            if (rows) {
                let optionChain = [];
                let expires = {};
                let strikes = {};
                rows.forEach(function (row) {
                    let op = common.parseOptionSymbol(row.symbol);
                    if (op) {
                        op.symbol = row.symbol;
                        optionChain.push(op);

                        let str = op.expireDate.getFullYear().toString();
                        str += common.padInt(op.expireDate.getMonth() + 1, 2);
                        str += common.padInt(op.expireDate.getDate(), 2);

                        expires[ str ] = 1;
                        strikes[ op.strike / 10000 ] = 1;
                    }
                });
                result.optionChain = optionChain;

                for (let key in expires) {
                    if (expires.hasOwnProperty(key)) {
                        result.expires.push(key);
                    }
                }
                result.expires.sort();


                for (let key in strikes) {
                    if (strikes.hasOwnProperty(key)) {
                        result.strikes.push(key);
                    }
                }
                result.strikes.sort();
            }

            return asyncCallback(err);
        });
    }, function (asyncCallback) {
        lookupDb.get(db.SELECT_NEWEST_BAR, [ underlier, common.DATA_TYPE._1DAY ], function (err, row) {
            if (row) {
                let path = db.getBarFilePath(underlier, underlier, common.DATA_TYPE._1DAY, row.fileId, row.start, false);
                let lines = fs.readFileSync(path, 'utf8').split('\n');
                let bar = lineToBar(lines[ lines.length - 1 ], false);
                result.underlierPrice = bar.close;
            }

            return asyncCallback(err);
        });
    } ], function (err) {
        if (err) {
            result.status = err;
        }
        return callback(err, result);
    });
};

module.exports = new Bars();