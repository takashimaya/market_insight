'use strict';

// Required modules
let fs = require('fs');
let async = require('async');
let sqlite3 = require('sqlite3');

let common = require('../lib/common');
let logger = require('../lib/logger');
let db = require('../lib/db');
let bars = require('../lib/bars');

let tradeKing = require('../lib/tradeking');

/**
 * The object to be exported.
 */
let Ticks = function () {
};

/**
 * Download tick data from the server. The data will be returned as the second parameter of callback.
 */
let requestTickData = function (symbol, beginTime, endTime, callback) {
    let q = "&trades=1&quotes=0";
    if (symbol.indexOf('OPTION') >= 0) {
        q = "&trades=1&quotes=1";
    }
    let query = "symbol=" + symbol + q + "&beginTime=" + beginTime + "&endTime=" + endTime;
    return common.sendHttpRequest('tickData', query, callback);
};

/**
 * Download the options currently available from the server. The data will be returned as the second parameter of callback.
 */
let requestOptionChain = function (symbol, callback) {
    let path = common.getDbPath(common.DATA_TYPE.TICK);
    common.checkDir(path);
    path += 'option_chains' + common.getPathSeparator();
    common.checkDir(path);

    path += symbol + '.txt';
    if (common.fileExists(path)) {
        let stat = fs.statSync(path);
        if (stat && stat.mtime) {
            let lastMonday = new Date();
            lastMonday.setHours(5, 0, 0, 0);
            let offset = lastMonday.getDay() - 1;
            if (offset === -1) {
                offset = 6;
            }
            lastMonday.setDate(lastMonday.getDate() - offset);

            if (lastMonday.getTime() <= stat.mtime.getTime()) {
                return async.setImmediate(function () {
                    return callback(null, fs.readFileSync(path, 'utf8'));
                });
            }
        }
    }

    // ActiveTick is fucked up. It doesn't return all option chains available. For example, as of Feb 22, 2017,
    // it doesn't return AVGO170616C230, even though it returns actual ticks if I request it.

    // I'll count on TradeKing for this part...
    tradeKing.getOptionChain(symbol, function (err, res) {
        if (err) {
            return callback(err);
        }

        fs.writeFileSync(path, res);
        return callback(null, res);
    });

    /*
     return common.sendHttpRequest('optionChain', 'symbol=' + symbol, function (err, res) {
     if (err) {
     return callback(err);
     }

     fs.writeFileSync(path, res);
     return callback(null, res);
     });
     */
};

/**
 * ActiveTick's option symbol is encoded such as 'OPTION:AVGO--170224P00177500'. This method takes an encoded string and
 * break it into components, which is packed into an object and returned.
 */
let decodeOptionSymbol = function (str) {
    let option = {};

    let pos = str.indexOf('-');

    option.underlier = str.substring(7, pos);
    option.shortSymbol = option.underlier;

    pos += 1;
    while (str.substring(pos, pos + 1) === '-') {
        ++pos;
    }

    option.shortSymbol += str.substring(pos, pos + 6);
    let y = 2000 + parseInt(str.substring(pos, pos + 2));
    pos += 2;
    let m = parseInt(str.substring(pos, pos + 2)) - 1;
    pos += 2;
    let d = parseInt(str.substring(pos, pos + 2));
    pos += 2;

    option.expireDate = new Date(y, m, d);

    let typeChar = str.substring(pos, pos + 1);
    option.shortSymbol += typeChar;
    option.type = (typeChar == 'C') ? common.OPTION_TYPE.CALL : common.OPTION_TYPE.PUT;
    pos += 1;

    option.strike = parseInt(str.substring(pos, pos + 7));
    option.shortSymbol += option.strike.toString();

    return option;
};


Ticks.prototype.requestTickUpdateAbort = function () {
    tickUpdateAbortRequested = true;
};

let updateBarProcess;
let updateBars = function (symbol) {

    if (updateBarProcess) {
        // Another process will take care of updating bars.
        // See the comments at the beginning of 'updateTicks' for details.
        return updateBarProcess(symbol);
    }

    bars.updateBars(symbol);
};

/**
 * Download new tick data from ActiveTick server.
 */
let tickUpdateCount = 0;
let tickUpdateAbortRequested = false;
Ticks.prototype.updateTicks = function (updateBarCallback) {
    tickUpdateAbortRequested = false;

    // In single processor mode, after ticks for a symbol get updated by 'updateTicks' method,
    // the symbol is put in 'updateBarQueue', which is then processed by 'updateBars' method.
    // In multiple processor mode, 'updateTicks' and 'updateBars' will run on different processes.
    // Here we need to use IPC to communicate between the two methods. The 'updateBarProcess'
    // callback mediates this.
    updateBarProcess = updateBarCallback;

    ++tickUpdateCount;

    // Note that startOfToday is a Date, whereas endOfToday is a timestamp
    let startOfToday = new Date();
    startOfToday.setHours(0, 0, 0, 0);
    let endOfToday = common.dateToTimestamp(startOfToday, '235959');

    // Contains the list of download targets
    let targets = [];

    // Ticker lookup table
    let lookupDb;
    let updateDb;

    let now = new Date();
    let stopwatch = now.getTime();

    let underlierCounts = 0;

    // Whether the market was open or not when update started.
    let wasMarketOpen = common.isMarketOpen(now);

    let marketOpenAbortMessage = 'Abort/Restart - market just opened.';
    let userAbortMessage = 'Abort/Restart - the user requested restart.';

    async.series([ function (asyncCallback) {
        /////////////////////////////////////////////////////////////////////////////////////////////////
        // Step 1. Get a handle of the ticker lookup DB.
        db.getTickerLookupDb(function (err, handle) {
            lookupDb = handle;
            return asyncCallback(err);
        });
    }, function (asyncCallback) {
        /////////////////////////////////////////////////////////////////////////////////////////////////
        // Step 2. Get a handle of the ticker update DB.
        db.getTickerUpdateDb(function (err, handle) {
            updateDb = handle;
            return asyncCallback(err);
        });
    }, function (asyncCallback) {
        /////////////////////////////////////////////////////////////////////////////////////////////////
        // Step 3. Read 'targets.csv' and create the target list
        let path = __dirname + common.getPathSeparator() + 'targets.csv';
        let lines = fs.readFileSync(path, 'utf8').split('\r\n');

        async.eachSeries(lines, function (line, asyncCallback2) {
            if (line === '') {
                return async.setImmediate(function () {
                    asyncCallback2(null);
                });
            }

            let tokens = line.split(',');

            let symbol = tokens[ 0 ];
            let active = parseInt(tokens[ 1 ]);

            if (active === 0) {
                return async.setImmediate(function () {
                    asyncCallback2(null);
                });
            }

            // Update Frequency:
            //    0 ... update during market close
            //    1 ... real time update
            //    2 ... real time update (underlier only. options will be updated during market close)
            let updateFrequency = (tokens.length > 2) ? parseInt(tokens[ 2 ]) : 0;

            // Options:
            //    0 ... no option chain
            //    1 ... full option chain (both weekly and monthly)
            //    2 ... monthly only
            let optionMode = (tokens.length > 3) ? parseInt(tokens[ 3 ]) : 0;

            let otmRange = (tokens.length > 4) ? parseFloat(tokens[ 4 ]) : 0.15;
            let itmRange = (tokens.length > 5) ? parseFloat(tokens[ 5 ]) : 0.1;

            updateDb.get(db.SELECT_TICK_UPDATE, [ symbol ], function (err, row) {
                if (err) {
                    return asyncCallback2(err);
                }

                let skip = false;

                let target = {
                    symbol: symbol,
                    option: false,
                    underlier: symbol,
                    shortSymbol: symbol,
                    entryExistsInUpdateDb: (row !== undefined),
                    lastUpdate: (row !== undefined) ? row.lastUpdate : new Date(2000, 0, 1)
                };

                if (row) {
                    if (!common.moreDataSinceLastUpdate(new Date(row.lastUpdate), false, false)) {
                        // console.log('Ticks have been updated: ' + symbol);
                        updateBars(target); // This should be unnecessary. But we let bar updating routine itself decide whether or not to skip this target.
                        skip = true;
                    }
                }

                if (!skip) {
                    // If market is open, update only those with updateFrequency being non-zero (i.e. real-time updates). If market is closed, update everything.
                    if ((updateFrequency !== 0) || !wasMarketOpen) {
                        ++underlierCounts;
                        targets.push(target);
                    }
                }

                if (optionMode === 0) {
                    return asyncCallback2(null);
                }

                lookupDb.get(db.SELECT_NEWEST_TICK, [ symbol ], function (err, row) {
                    if (err) {
                        return asyncCallback2(err);
                    }

                    let refPrice = 0;
                    if (row) {
                        let path = db.getTickFilePath(symbol, symbol, row.fileId, row.start, false);
                        let ticks = fs.readFileSync(path, 'utf8').split('\n');
                        let tick = ticks[ ticks.length - 1 ];
                        let tokens = tick.split(',');
                        refPrice = parseFloat(tokens[ 2 ]) * 100;
                    }

                    // Get all the options available for the symbol and create a target out of each option.
                    requestOptionChain(symbol, function (err, res) {
                        if (err) {
                            return asyncCallback2(err);
                        }

                        if (!res) {
                            // No option is available...
                            return asyncCallback2(null);
                        }

                        let activeOptionCount = 0;
                        let optionChain = res.split('\r\n');

                        async.eachSeries(optionChain, function (option, asyncCallback3) {
                            if (!option) {
                                return async.setImmediate(function () {
                                    asyncCallback3(null);
                                });
                            }

                            let op = decodeOptionSymbol(option);

                            let skip = false;
                            if (wasMarketOpen && (updateFrequency !== 1)) {
                                skip = true;
                            }

                            if (op.expireDate.getTime() - startOfToday.getTime() > 365 * 24 * 3600 * 1000) {
                                // Ignore if expire date is more than a year away...
                                skip = true;
                            }

                            if (optionMode === 2) {
                                // Monthly options expires on a Friday that falls between 16 and 22.
                                let d = op.expireDate.getDate();
                                if ((d < 16) || (d > 22)) {
                                    skip = true;
                                }

                                if (op.expireDate.getDay() !== 5) {
                                    skip = true;
                                }
                            }

                            if (refPrice > 0) {
                                let distance = (op.strike - refPrice) / refPrice;
                                if (op.type === common.OPTION_TYPE.PUT) {
                                    distance *= -1;
                                }

                                if ((distance > otmRange) || ((-1) * distance > itmRange)) {
                                    skip = true;
                                }
                            }

                            // If the strike is above $500, we take only those with the last digits (in cents) being either 000 or 500.
                            if ((op.strike > 50000) && ((op.strike.toString().slice(-3) !== '000') && ((op.strike.toString().slice(-3) !== '500')))) {
                                skip = true;
                            }

                            if ((op.underlier === 'SPY') || (op.underlier === 'QQQ') || (op.underlier === 'IWM')) {
                                if ((op.strike.toString().slice(-2) !== '00')) {
                                    skip = true;
                                }
                            }

                            if (skip) {
                                return async.setImmediate(function () {
                                    asyncCallback3(null);
                                });
                            }

                            updateDb.get(db.SELECT_TICK_UPDATE, [ op.shortSymbol ], function (err, row) {
                                if (err) {
                                    return asyncCallback3(err);
                                }

                                let item = {
                                    symbol: option,
                                    option: true,
                                    underlier: op.underlier,
                                    expireDate: op.expireDate,
                                    shortSymbol: op.shortSymbol,
                                    updateMode: updateFrequency,
                                    entryExistsInUpdateDb: (row !== undefined),
                                    lastUpdatelastUpdate: (row !== undefined) ? row.lastUpdate : new Date(2000, 0, 1)
                                };

                                if (row) {
                                    if (!common.moreDataSinceLastUpdate(new Date(row.lastUpdate), true, false)) {
                                        // console.log('Ticks have been updated: ' + op.shortSymbol);
                                        updateBars(item); // This should be unnecessary. But we let bar updating routine itself decide whether or not to skip this target.
                                        return asyncCallback3(null);
                                    }
                                }

                                targets.push(item);
                                ++activeOptionCount;

                                return asyncCallback3(null);
                            });
                        }, function (err) {
                            if (activeOptionCount > 0) {
                                console.log('# of active options for ' + symbol + " = " + activeOptionCount);
                            }
                            return asyncCallback2(err);
                        });
                    });
                });
            });

        }, function (err) {
            return asyncCallback(err);
        });
    }, function (asyncCallback) {
        /////////////////////////////////////////////////////////////////////////////////////////////////
        // Step 4. Check the lookup table to determine the start time for download for each target
        async.eachSeries(targets, function (target, asyncCallback2) {
            // Set the default start time for download...
            let downloadStart = '20161201000000';
            if (target.option) {
                // The assumption is that an option becomes available at least 42 days (6 weeks)
                // before expire date. If this date (expire date - 42 days) is before today, we start
                // from there. Otherwise, start from today.
                let date = new Date(target.expireDate);
                date.setDate(date.getDate() - 42);
                if (date.getTime() > startOfToday.getTime()) {
                    date = new Date(startOfToday);
                }
                downloadStart = common.dateToTimestamp(date, '000000');
            }

            lookupDb.get(db.SELECT_NEWEST_TICK, [ target.shortSymbol ], function (err, row) {
                if (err) {
                    return asyncCallback2(err);
                }

                let fileId = 0;
                let fileStart = downloadStart;
                let creatingNewFile = true;

                if (row) {
                    // When we request data to the server, the resolution of start time is only in seconds
                    // (not in milliseconds). Because of this, we make sure that the ticks belonging to
                    // the same second are saved at once. This can be achieved only by discarding the ticks
                    // belonging to the last second from the retrieved data, as we don't know if there are
                    // more ticks in the same second that has not been retrieved.
                    // To calculate start time for the next request, we first get the last second for which
                    // the saved data exists, and add 1 sec to it.
                    let end = common.timestampToDate(row.end);
                    end.setSeconds(end.getSeconds() + 1);
                    downloadStart = common.dateToTimestamp(end);
                    if (row.count < db.TICK_FILE_MAX_LINES) {
                        // The last file has room for more data. Downloaded data will be appended to the file.
                        creatingNewFile = false;
                        fileId = row.fileId;
                        fileStart = row.start;
                    }
                    else {
                        // We will create a new file
                        fileId = row.fileId + 1;
                        fileStart = downloadStart;
                    }
                }

                target.downloadStart = downloadStart;
                target.fileId = fileId;
                target.fileStart = fileStart;
                target.creatingNewFile = creatingNewFile;

                return asyncCallback2(null);
            });
        }, function (err) {
            return asyncCallback(err);
        });
    }, function (asyncCallback) {
        /////////////////////////////////////////////////////////
        // Step 5. Download tick data from ActiveTick server...
        let targetCount = targets.length;
        let targetIndex = 0;
        let lastUnderlier = '';

        async.eachSeries(targets, function (target, asyncCallback2) {

            if (tickUpdateAbortRequested) {
                return asyncCallback2(userAbortMessage);
            }

            let now = new Date();
            if (!wasMarketOpen && common.isMarketOpen(now)) {
                // Let's abort and start again, 'cause the market has just opened. Now we need 'real time' update.
                return asyncCallback2(marketOpenAbortMessage);
            }

            if ((++targetIndex % 500 === 1) || (target.underlier !== lastUnderlier)) {
                let msg = target.shortSymbol + ' [' + targetIndex + '/' + targetCount + '] (' + tickUpdateCount + ') ';
                msg += Math.round((now.getTime() - stopwatch) / 1000) + ' sec. @' + now.toLocaleTimeString();
                logger.add(logger.TYPE.UPDATE, 'Ticks', msg);
                lastUnderlier = target.underlier;
            }

            let ticks = [];
            if (!target.creatingNewFile) {
                // console.log('Loading local data...');
                // Read data from the existing file to which downloaded data will be appended.
                let path = db.getTickFilePath(target.shortSymbol, target.underlier, target.fileId, target.fileStart, false);
                ticks = fs.readFileSync(path, 'utf8').split('\n');
            }

            let downloadStart = target.downloadStart;

            target.lastUpdate = new Date().getTime();

            let totalPeriodInSec = (target.lastUpdate - common.timestampToDate(target.downloadStart).getTime()) / 1000;
            let totalTicks = 0;

            let complete = false;
            async.whilst(function () {
                return complete === false;
            }, function (asyncCallback3) {
                // console.log('Request data from ' + downloadStart);
                requestTickData(target.symbol, downloadStart, endOfToday, function (err, res) {
                    if (err) {
                        return asyncCallback3(err);
                    }

                    let lines = res.split('\r\n');
                    let lineCount = lines.length;

                    // Last line is always empty. Let's get rid of it.
                    while ((lineCount > 0) && (lines[ lineCount - 1 ] === '')) {
                        --lineCount;
                    }

                    // If no data exists, the server returns '0'.
                    if ((lineCount === 1) && (lines[ 0 ] === '0')) {
                        --lineCount;

                        // It appears the server returns '0' if there is no data on the day specified by start time
                        // (event though there are some data in the subsequent days). We increment start time by 1 day
                        // and resend the server request.
                        let temp = common.timestampToDate(downloadStart);
                        temp.setDate(temp.getDate() + 1);
                        temp.setHours(0, 0, 0, 0);
                        if (startOfToday.getTime() >= temp.getTime()) {
                            downloadStart = common.dateToTimestamp(temp);
                            // console.log('No data exists. Advance clock: ' + downloadStart);
                            return asyncCallback3();
                        }
                        else {
                            // Start time was today. So we are up to date and there is in fact no more data now.
                            complete = true;
                        }
                    }

                    // When we request data to the server, the resolution of start time is only in seconds
                    // (not in milliseconds). Because of this, we make sure that the ticks belonging to
                    // the same second are saved at once. This can be achieved only by discarding the ticks
                    // belonging to the last second from the retrieved data, as we don't know if there are
                    // more ticks in the same second that has not been retrieved.
                    let lastSecond = 0;

                    if (lineCount > 0) {
                        let line = lines[ lineCount - 1 ];
                        let tokens = line.split(',');
                        lastSecond = common.truncateMilliseconds(tokens[ 1 ]);
                        // NOTE: If there is only one valid line, it will be discarded so the data is empty.
                        --lineCount;
                    }

                    while (lineCount > 0) {
                        let line = lines[ lineCount - 1 ];
                        let tokens = line.split(',');
                        if (lastSecond === common.truncateMilliseconds(tokens[ 1 ])) {
                            // This data belongs to the last second, thus to be discarded.
                            --lineCount;
                        }
                        else {
                            // Update start time for the next server request.
                            downloadStart = lastSecond;
                            break;
                        }
                    }

                    totalTicks += lineCount;

                    let d = common.timestampToDate(downloadStart);

                    let clock = new Date();
                    clock.setHours(clock.getHours() + 3); // PDT/PST to EDT/EST
                    let secondsToPresent = (clock.getTime() - d.getTime()) / 1000;

                    if ((lineCount === 0) || (secondsToPresent < 60)) {
                        // The target is up to date. Let's wrap it up.
                        complete = true;
                    }

                    // If the current time is 9 pm or later, +3 hours moves to the next day.
                    // We need to move back first.
                    clock.setHours(clock.getHours() - 3);

                    clock.setHours(16, 0, 0, 0);
                    let secondsToMarketClose = (clock.getTime() - d.getTime()) / 1000;

                    if (target.option) {
                        if ((totalPeriodInSec > 6 * 3600) && (totalTicks < 1000)) {
                            // Ticks are very sparce for this option...
                            if (secondsToMarketClose < 1800) {
                                // The last data (after subtracting the last second) is close enough to the market close.
                                // console.log('MARKET CLOSE: ' + target.shortSymbol);
                                complete = true;
                            }
                        }
                        else {
                            if (secondsToMarketClose < 60) {
                                // The last data (after subtracting the last second) is close enough to the market close.
                                // console.log('MARKET CLOSE: ' + target.shortSymbol);
                                complete = true;
                            }
                        }
                    }

                    let lastHour = -1;
                    let lastMin = -1;
                    let lastBid = -1;
                    let lastAsk = -1;
                    for (let i = 0; i < lineCount; ++i) {
                        let line = lines[ i ];
                        let tokens = line.split(',');
                        if (tokens[ db.TICK_COLUMN.TYPE ] === 'Q') {
                            // Ticks for an option contains quotes. They can be way too many so we'll compress them...
                            let d = common.timestampToDate(tokens[ 1 ]);
                            let hour = d.getHours();
                            let min = d.getMinutes();
                            let bid = parseFloat(tokens[ db.TICK_COLUMN.Q_BID ]);
                            let ask = parseFloat(tokens[ db.TICK_COLUMN.Q_ASK ]);
                            if ((bid === 0.00) && (ask === 0.00)) {
                                // Get rid of garbage...
                                continue;
                            }

                            if ((lastHour === hour) && (lastMin === min) && (lastBid === bid) && (lastAsk === ask)) {
                                continue;
                            }

                            lastHour = hour;
                            lastMin = min;
                            lastBid = bid;
                            lastAsk = ask;
                        }
                        ticks.push(line);
                    }

                    if ((ticks.length > db.TICK_FILE_MAX_LINES) || (complete === true)) {
                        // Save all ticks into a file and increment file index. Also updateTicks the lookup table.

                        let startTime = target.fileStart;
                        let endTime = 0;
                        let fileId = target.fileId;

                        let tickLen = ticks.length;
                        if (tickLen !== 0) {

                            let first = 0;
                            let last = (tickLen > db.TICK_FILE_MAX_LINES) ? db.TICK_FILE_MAX_LINES : tickLen;

                            while (true) {
                                let lines = '';

                                endTime = common.truncateMilliseconds(parseInt(ticks[ last - 1 ].split(',')[ 1 ]));
                                // Make sure all the ticks belonging to the last second will be saved together. BTW, this check has already been done
                                // for the end of the array. So, if we reach the end, we are good.
                                while (last < tickLen) {
                                    if (common.truncateMilliseconds(parseInt(ticks[ last ].split(',')[ 1 ])) === endTime) {
                                        ++last;
                                    }
                                    else {
                                        break;
                                    }
                                }

                                if (tickLen - last < db.TICK_FILE_MAX_LINES) {
                                    // The remaining part is less than a full page. Let's combine it.
                                    last = tickLen;
                                }

                                for (let i = first; i < last; ++i) {
                                    lines += ticks[ i ] + ((i < last - 1) ? '\n' : '');
                                }

                                let path = db.getTickFilePath(target.shortSymbol, target.underlier, fileId, startTime, true);
                                fs.writeFileSync(path, lines);

                                let count = last - first;
                                if (target.creatingNewFile) {
                                    lookupDb.run(db.INSERT_TICK, [ target.shortSymbol, startTime, endTime, count, fileId ]);
                                }
                                else {
                                    lookupDb.run(db.UPDATE_TICK, [ endTime, count, target.shortSymbol, fileId ]);
                                }

                                if (last === tickLen) {
                                    // All ticks are saved to file(s). Done.
                                    break;
                                }

                                // We have more data to save. Let's update parameters for the next batch.
                                target.creatingNewFile = true;
                                ++fileId;

                                let end = common.timestampToDate(endTime);
                                end.setSeconds(end.getSeconds() + 1);
                                startTime = common.dateToTimestamp(end);
                                endTime = 0;

                                first = last;
                                last = last + db.TICK_FILE_MAX_LINES;
                                if (last > tickLen) {
                                    last = tickLen;
                                }
                            }
                        }

                        target.creatingNewFile = true;
                        target.fileStart = downloadStart;
                        target.fileId = fileId + 1;
                        ticks = [];
                    }

                    return asyncCallback3();
                });
            }, function (err) {
                if (err) {
                    return asyncCallback2(err);
                }

                // Have update bar data, but we don't wait for it to complete.
                updateBars(target);

                // Ticks are updated successfully for the target. Finally, we update 'lastUpdate'.
                if (target.entryExistsInUpdateDb) {
                    updateDb.run(db.UPDATE_TICK_UPDATE, [ target.lastUpdate, target.shortSymbol ], function (err) {
                        return asyncCallback2(err);
                    });
                }
                else {
                    updateDb.run(db.INSERT_TICK_UPDATE, [ target.shortSymbol, target.lastUpdate ], function (err) {
                        return asyncCallback2(err);
                    });
                }
            });
        }, function (err) {
            return asyncCallback(err);
        });
    } ], function (err) {
        let timeMsg = ' (started @' + now.toLocaleTimeString() + ', finished @' + new Date().toLocaleTimeString() + ')';

        if (err) {
            if ((err === marketOpenAbortMessage) || (err === userAbortMessage)) {
                logger.add(logger.TYPE.INFO, 'Ticks', err + timeMsg);
                return setTimeout(function () {
                    Ticks.prototype.updateTicks(updateBarCallback);
                }, 10000);
            }
            logger.add(logger.TYPE.ERROR, 'Ticks', 'Update failed: ' + err + timeMsg);
        }
        else {
            logger.add(logger.TYPE.INFO, 'Ticks', 'Update complete.' + timeMsg);
            let waitMin = 30;
            let date1 = new Date();
            let date2 = new Date();
            date2.setMinutes(date2.getMinutes() + waitMin);
            setTimeout(function () {
                Ticks.prototype.updateTicks(updateBarCallback);
            }, common.isMarketOpen(date1) || common.isMarketOpen(date2)? 1000 : waitMin * 60000);
        }
    });
};

Ticks.prototype.updateSettings = function (settings) {
    let lines = '';
    let len = settings.length;
    for (let i = 0; i < len; ++i) {
        let entry = settings[ i ];
        lines += entry.symbol + ',' + entry.active + ',' + entry.updateMode + ',' + entry.optionMode;

        // Option Mode:
        //    0 ... no option chain
        //    1 ... full option chain (both weekly and monthly)
        //    2 ... monthly only
        if (entry.optionMode !== 0) {
            let otmRange = 0.15;
            let itmRange = 0.1;

            if (entry.otmRange) {
                otmRange = entry.otmRange;
            }
            if (entry.itmRange) {
                itmRange = entry.itmRange;
            }

            lines += ',' + otmRange + ',' + itmRange;
        }

        if (i !== len - 1) {
            lines += '\r\n';
        }
    }

    let path = __dirname + common.getPathSeparator() + 'targets.csv';
    fs.writeFileSync(path, lines);

    Ticks.prototype.requestTickUpdateAbort();
};

Ticks.prototype.getSettings = function () {
    let path = __dirname + common.getPathSeparator() + 'targets.csv';
    let lines = fs.readFileSync(path, 'utf8').split('\r\n');

    let settings = [];
    lines.forEach(function (line) {
        let tokens = line.split(',');

        let symbol = tokens[ 0 ];
        let active = parseInt(tokens[ 1 ]);
        let updateMode = parseInt(tokens[ 2 ]);
        let optionMode = parseInt(tokens[ 3 ]);

        let entry = {
            symbol: symbol,
            active: active,
            updateMode: updateMode,
            optionMode: optionMode
        };

        if (entry.optionMode !== 0) {
            let otmRange = 0.2;
            let itmRange = 0.1;

            if (tokens.length > 4) {
                otmRange = parseFloat(tokens[ 4 ]);
            }
            if (tokens.length > 5) {
                itmRange = parseFloat(tokens[ 5 ]);
            }

            entry.otmRange = otmRange;
            entry.itmRange = itmRange;
        }

        settings.push(entry);
    });

    return settings;
};

Ticks.prototype.startSlaveMode = function () {
    // In slave mode, we don't query data to ActiveTick server. Instead, we post requests to a Dropbox folder (let's call it the exchange folder),
    // which is processed by the master. There are two files to be put in the exchange folder. One is '_Slave.txt', which is created and written
    // only by the slave. It should look like:
    //    AVGO 20170303122400
    //    AVGO170303C24000 20170303105200
    //     :
    //  So it tells the server where new bars should start for each symbol.
    // The other is '_Master.txt'
};

Ticks.prototype.startMasterMode = function () {
    // Starts the master mode. See the comments in 'startSlaveMode' for the details of how this should work.
};

module.exports = new Ticks();