'use strict';

/**
 * Module dependencies.
 */
let ticks = require('../lib/ticks');
let bars = require('../lib/bars');

let type = process.argv[ 2 ];
let master = ((process.argv.length >= 4) && (process.argv[ 3 ] === 'master')) ? true : false;
if (type === 'ticks') {
    ticks.updateTicks(function (symbol) {
        process.send(symbol);
    });
}
else if (type === 'bars') {
    bars.updateBarsForFutures();
    process.on('message', function (symbol) {
        bars.updateBars(symbol);
    });
}
