'use strict';

/**
 * Module dependencies.
 */
let app = require('../app');
let debug = require('debug')('marketinsight:server');
let process = require('process');
let http = require('http');
let io = require('socket.io');
let cron = require('cron');
let fs = require('fs');

let common = require('../lib/common');
let logger = require('../lib/logger');

let ticks = require('../lib/ticks');
let bars = require('../lib/bars');

var port = '3300';
app.set('port', '3300');

var httpServer = http.createServer(app);
var ioServer = new io(httpServer);

httpServer.listen(port);
httpServer.on('error', function (error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
});

httpServer.on('listening', function () {
    var addr = httpServer.address();
    var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
    debug('Listening on ' + bind);
});

ioServer.on('connection', function (socket) {
    socket.on('start', function () {
        logger.addSocket(socket);
    });
    socket.on('stop', function () {
        logger.removeSocket(socket);
    });
    socket.on('disconnect', function () {
        logger.removeSocket(socket);
    });
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Start Worker Processes

let startWorkerProcess = function () {
    let master = false;
    let singleProcess = false;

    let argvLen = process.argv.length;
    for (let i = 2; i < argvLen; ++i) {
        let param = process.argv[ i ];
        if (param === 'master') {
            master = true;
        }
        else if (param === 'single') {
            singleProcess = true;
        }
    }

    if (master) {
        if (singleProcess) {
            ticks.updateTicks();
            bars.updateBarsForFutures();
        }
        else {
            const childProcess = require('child_process');

            let argv = [];
            // argv = [ '--debug-brk=' + 53462, '--expose_debug_as=v8debug' ]
            let bars = childProcess.fork(__dirname + common.getPathSeparator() + 'launcher.js', [ 'bars' ], { execArgv: argv });

            argv = [];
            // argv = [ '--debug-brk=' + 53462, '--expose_debug_as=v8debug' ]
            let ticks = childProcess.fork(__dirname + common.getPathSeparator() + 'launcher.js', [ 'ticks' ], { execArgv: argv });

            ticks.on('message', function (m) {
                bars.send(m);
            });

            process.on('SIGINT', function () {
                bars.kill();
                ticks.kill();
                console.log('END.');
            });
        }
    }
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Start ActiveTick http server proxy service...

let monitorKillStartProxy = function (immediate) {
    if (!immediate) {

    }
};

var serverDir = __dirname + common.getPathSeparator() + '..' + common.getPathSeparator() + 'activetick_server';
var serverExePath = '';
if (common.isWindows()) {
    serverDir += common.getPathSeparator() + 'windows';
    serverExePath = serverDir + common.getPathSeparator() + 'ActiveTickFeedHttpServer.exe';
}
else {
    serverDir += common.getPathSeparator() + 'mac';
    serverExePath = serverDir + common.getPathSeparator() + 'ActiveTickFeedHttpServer';
}

if (common.isWindows()) {
    startWorkerProcess();
}
else {
    // @formatter:off
    /*
    var exec = require('child_process').exec;
    var cmd = 'kill $(lsof -i :3200|grep ActiveTi|cut -d" " -f2)';

    exec(cmd, function (error, stdout, stderr) {
        // console.log(stdout);
        // console.log(stderr);

        const spawn = require('child_process').spawn;
        const activeTick = spawn(serverExePath,
                                 [ '127.0.0.1', '3200', 'activetick1.activetick.com', '5cebbc79903749c2bba2d6ecf6c8574c', 'ttsuboi', '0r30r3sag1' ],
                                 { cwd: serverDir });

        activeTick.stdout.on('data', (data) => {
            console.log(`stdout: ${data}`);
        });

        activeTick.stderr.on('data', (data) => {
            console.log('ActiveTickServer ERROR: ' + `stderr: ${data}`);
        });

        activeTick.on('close', (code) => {
            console.log(`child process exited with code ${code}`);
        });


        startWorkerProcess();
    });
    */
    // @formatter:on

    startWorkerProcess();
}
