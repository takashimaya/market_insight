'use strict';

let express = require('express');
let router = express.Router();

router.get('/:themes', function (req, res, next) {
    res.render('quickview', { theme: req.params.themes });
});

module.exports = router;
