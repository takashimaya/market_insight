'use strict';

let express = require('express');
let router = express.Router();

let db = require('../lib/db');

router.get('/', function (req, res, next) {
    res.render('settings', { title: 'Express' });
});

router.get('/get', function (req, res, next) {
    let settings = db.getSettings();
    let result = {
        status: 'SUCCESS',
        data: settings
    }
    res.send(JSON.stringify(result));
});

module.exports = router;
