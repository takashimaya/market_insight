'use strict';

let express = require('express');
let router = express.Router();

let async = require('async');

let common = require('../lib/common');
let bars = require('../lib/bars');
let ticks = require('../lib/ticks');
let fidelity = require('../lib/fidelity');

router.get('/symbols', function (req, res, next) {
    let result = {
        status: 'SUCCESS',
        data: []
    };

    // Get the list of underliers for which options were traded over a year...
    let startDate = new Date();
    startDate.setFullYear(startDate.getFullYear() - 1);
    fidelity.getTradedUnderliers(startDate, function (err, symbolsWithPos) {
        if (err) {
            result.status = err;
        }
        else {
            let symbols = ticks.getSettings();
            symbols.forEach(function (item) {
                if (!item.active) {
                    return;
                }

                // Brute-force search
                let found = false;
                let len = symbolsWithPos.length;
                for (let i = 0; i < len; ++i) {
                    if (symbolsWithPos[ i ].underlier === item.symbol) {
                        found = true;
                        break;
                    }
                }

                let entry = {
                    symbol: item.symbol,
                    withPos: found
                };

                result.data.push(entry);
            });
        }

        return res.send(JSON.stringify(result));
    });
});

router.get('/optionchain', function (req, res, next) {
    let result = {
        status: 'SUCCESS',
        data: {}
    };

    let underlier = req.query.underlier;
    if (!underlier) {
        result.status = 'ERROR: underlier is not specified';
        return res.send(JSON.stringify(result));
    }

    let resolution = req.query.resolution;
    if (resolution === '5m') {
        resolution = common.DATA_TYPE._5MIN;
    }
    else if (resolution === '30m') {
        resolution = common.DATA_TYPE._30MIN;
    }
    else if (resolution === '1d') {
        resolution = common.DATA_TYPE._1DAY;
    }
    else {
        result.status = 'ERROR: resolution is not specified';
        return res.send(JSON.stringify(result));
    }

    let end = common.dateToTimestamp(new Date());
    let start = common.dateToTimestamp(new Date(2016, 0, 1));
    async.parallel([ function (asyncCallback) {
        bars.getOptionChain(underlier, start, end, resolution, function (err, optionChain) {
            if (err) {
                result.status = err;
            }
            else {
                result.data.expires = optionChain.expires;
                result.data.strikes = optionChain.strikes;
                result.data.optionChain = optionChain.optionChain;
                result.data.underlierPrice = optionChain.underlierPrice;
            }

            return asyncCallback(err);
        });
    }, function (asyncCallback) {
        fidelity.getPositionsForUnderlier(underlier, function (err, positions) {
            if (err) {
                result.status = err;
            }
            else {
                result.data.positions = positions;
            }

            return asyncCallback(err);
        })
    } ], function (err) {
        return res.send(JSON.stringify(result));
    });
});

router.get('/', function (req, res, next) {
    let result = {
        status: 'SUCCESS',
        data: {}
    };

    let symbols = req.query.symbols;
    if (symbols) {
        symbols = symbols.split(',');
    }
    else {
        result.status = 'ERROR: symbols are not specified';
        return res.send(JSON.stringify(result));
    }

    let start = req.query.start;
    if (start) {
        start = parseInt(start);
    }
    else {
        result.status = 'ERROR: start (yyyyMMddhhmmss) is not specified';
        return res.send(JSON.stringify(result));
    }

    let end = req.query.end;
    if (end) {
        end = parseInt(end);
    }
    else {
        result.status = 'ERROR: end (yyyyMMddhhmmss) is not specified';
        return res.send(JSON.stringify(result));
    }

    let resolution = req.query.resolution;
    if (resolution === '5m') {
        resolution = common.DATA_TYPE._5MIN;
    }
    else if (resolution === '30m') {
        resolution = common.DATA_TYPE._30MIN;
    }
    else if (resolution === '1d') {
        resolution = common.DATA_TYPE._1DAY;
    }
    else {
        result.status = 'ERROR: resolution is not specified';
        return res.send(JSON.stringify(result));
    }

    bars.getBars(symbols, start, end, resolution, function (err, data) {
        if (err) {
            result.status = err;
        }
        else {
            result = data;
        }
        res.send(JSON.stringify(result));
    });
});

module.exports = router;
