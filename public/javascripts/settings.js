'use strict';

/**
 * The app
 */
let app = angular.module('app', [ 'ui.bootstrap', 'util', 'clockToDate', 'serverLog' ]);

/**
 * The controller for Balances page.
 */
app.controller('SettingsCtrl', [ '$scope', '$http', 'util', 'serverLog', function ($scope, $http, util, serverLog) {
    // Holds the log events from the server.
    $scope.log = [];
    $scope.singleLineLog = '';
    serverLog.start(function (contents) {
        if (($scope.log.length > 0) && (contents.type === 'update')) {
            $scope.log.pop();
        }
        else {
            while ($scope.log.length >= 25) {
                $scope.log.shift();
            }
        }

        $scope.$apply(function () {
            $scope.log.push(contents);
            $scope.singleLineLog = ($scope.log.length > 0) ? $scope.log[ $scope.log.length - 1 ] : '';
        });
    });

    $scope.settings = [];

    $scope.getSettings = function () {
        let config = {
            method: 'GET',
            url: 'http://localhost:3300/settings/get',
            params: {}
        };
        $http(config).success(function (result) {
            if (result.status !== 'SUCCESS') {
                $scope.error = status;
            }
            else {
                $scope.settings = result.data;
            }
        }).error(function (data, status) {
            $scope.error = status;
        });
    };

    $scope.getSettings();

    $scope.commitSettings = function () {

    };
} ]);