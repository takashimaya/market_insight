'use strict';

/**
 * The app
 */
let app = angular.module('app', [ 'ui.bootstrap', 'util', 'timestamp', 'marketTimer', 'timestampToDate' ]);

/**
 * The controller for Balances page.
 */
app.controller('OptionsCtrl', [ '$scope', '$location', '$http', 'util', 'timestamp', function ($scope, $location, $http, util, timestamp) {
    // 'underliers' holds the list of underliers for which the data is available.
    // This list is shown on the sidebar. The first thing for the user to do is to select
    // an underlier from it. Then, the options for the underliers will show up on the main panel.
    $scope.underliers = [];
    $scope.underlierFilter = '';

    // 'optionChain' contains all options available for the selected underlier.
    $scope.optionChain = [];
    $scope.optionChainWithPositions = [];
    $scope.optionSelector = [];

    $scope.showPositionSelector = true;
    $scope.positionFilter = {
        symbol: '',
        start: '',
        end: ''
    };

    $scope.getUnderliers = function () {
        let config = {
            method: 'GET',
            url: 'http://localhost:3300/bars/symbols',
            params: {}
        };
        $http(config).success(function (result) {
            if (result.status !== 'SUCCESS') {
                $scope.error = status;
            }
            else {
                $scope.underliers = result.data;
                $scope.selectUnderlier('AVGO'); // Or the first element in the $scope.symbols...
            }
        }).error(function (data, status) {
            $scope.error = status;
        });
    };
    $scope.getUnderliers();

    // Get the available option chains for the selected symbol...
    $scope.selectUnderlier = function (underlier) {
        let config = {
            method: 'GET',
            url: 'http://localhost:3300/bars/optionchain',
            params: {
                underlier: underlier,
                resolution: '5m'
            }
        };
        $http(config).success(function (result) {
            if (result.status !== 'SUCCESS') {
                $scope.error = status;
            }
            else {
                $scope.globalSettings.underlier = underlier;
                $scope.globalSettings.underlierPrice = result.data.underlierPrice / 100;

                $scope.expires = result.data.expires;

                $scope.optionChain = [];
                result.data.optionChain.forEach(function (item) {
                    let position = null;
                    let posLen = result.data.positions.length;
                    for (let i = 0; i < posLen; ++i) {
                        let p = result.data.positions[ i ];
                        if (p.symbol === item.symbol) {
                            position = p;
                            break;
                        }
                    }
                    $scope.optionChain.push({
                                                item: item,
                                                selected: false,
                                                position: position
                                            });
                });

                $scope.resetOptionFilters();
            }
        }).error(function (data, status) {
            $scope.error = status;
        });
    };

    $scope.resetOptionFilters = function () {
        $scope.positionFilter.symbol = '';
        $scope.positionFilter.start = '';
        $scope.positionFilter.end = timestamp.getDatePart(timestamp.fromDate(new Date()));

        // Unselect all options.
        let optionChainLen = $scope.optionChain.length;
        for (let k = 0; k < optionChainLen; ++k) {
            let option = $scope.optionChain[ k ];
            option.selected = false;
        }

        $scope.updatePositionFilter(true);

        $scope.optionSelector = [];
        let expireLen = $scope.expires.length;
        for (let j = expireLen - 1; j >= 0; --j) {
            let expire = $scope.expires[ j ];
            let expireTime = timestamp.toDate(expire).getTime();

            let optionsWithSameExpire = [];

            let optionChainLen = $scope.optionChain.length;
            for (let k = 0; k < optionChainLen; ++k) {
                let option = $scope.optionChain[ k ];
                let op = util.parseOptionSymbol(option.item.symbol);
                if (op.expireDate.getTime() === expireTime) {
                    optionsWithSameExpire.push({
                                                   name: op.optionType + op.strike,
                                                   item: option
                                               });
                }
            }

            if (optionsWithSameExpire.length > 0) {
                optionsWithSameExpire.sort(function (a, b) {
                    if (a.name < b.name) {
                        return -1;
                    }
                    else if (a.name > b.name) {
                        return 1;
                    }
                    else {
                        return 0;
                    }
                });
                $scope.optionSelector.push({
                                               name: expire,
                                               expended: false,
                                               options: optionsWithSameExpire
                                           });
            }
        }

        $scope.update();
    };

    $scope.updatePositionFilter = function (select) {
        let symbol = $scope.positionFilter.symbol;

        let dateRegExp = /^([0-9]{4})([0-9]{2})([0-9]{2})/;
        let parseDate = function (dateStr) {
            let reg = dateRegExp.exec(dateStr);
            if (reg) {
                let year = parseInt(reg[ 1 ]);
                let month = parseInt(reg[ 2 ]);
                let day = parseInt(reg[ 3 ]);
                return new Date(year, month - 1, day);
            }
            return -1;
        };

        let start = parseDate($scope.positionFilter.start);
        if (start === -1) {
            if ($scope.positionFilter.start !== '') {
                // It is likely that the user is in the middle of typing.
                // Let's not update...
                return;
            }
        }
        else {
            start = start.getTime();
        }

        let end = parseDate($scope.positionFilter.end);
        if (end === -1) {
            if ($scope.positionFilter.end !== '') {
                // It is likely that the user is in the middle of typing.
                // Let's not update...
                return;
            }
        }
        else {
            end.setHours(23, 59, 59);
            end = end.getTime();
        }

        $scope.optionChainWithPositions = [];
        $scope.optionChain.forEach(function (option) {
            if (!option.position) {
                return;
            }

            let symbolMatched = !symbol || (option.item.symbol.indexOf(symbol) > 0);
            let startInRange = (start === -1) || (timestamp.toDate(option.position.startDate).getTime() <= start);
            let endInRange = (end === -1) || (option.position.endDate === 0) || (end <= timestamp.toDate(option.position.endDate).getTime());
            if (symbolMatched && startInRange && endInRange) {
                if (select) {
                    option.selected = true;
                }
                $scope.optionChainWithPositions.push(option);
            }
        });

        $scope.optionChainWithPositions.sort(function (a, b) {
            if ((a.position.endDate === 0) && (b.position.endDate === 0)) {
                return 0;
            }
            if (a.position.endDate === 0) {
                return -1;
            }
            if (b.position.endDate === 0) {
                return 1;
            }
            return b.position.endDate - a.position.endDate;
        });
    };

    $scope.expandSelector = function (targetOption) {
        let len = $scope.optionSelector.length;
        for (let j = 0; j < len; ++j) {
            let group = $scope.optionSelector[ j ];
            let groupLen = group.options.length;
            for (let i = 0; i < groupLen; ++i) {
                let option = group.options[ i ];
                if (option.item === targetOption) {
                    group.expanded = true;
                    return;
                }
            }
        }
    };

    $scope.update = function () {
        let timeParams = timestamp.getServerRequestParams($scope.globalSettings.period);

        let positionMap = {};

        let targets = '';
        $scope.optionChain.forEach(function (option) {
            if (option.selected) {
                if (targets) {
                    targets += ',';
                }
                targets += option.item.symbol;

                if (option.position) {
                    positionMap[ option.item.symbol ] = option.position.events;
                }
            }
        });

        let config = {
            method: 'GET',
            url: 'http://localhost:3300/bars',
            params: {
                symbols: targets,
                start: timeParams.start,
                end: timeParams.end,
                resolution: timeParams.resolution
            }
        };
        $http(config).success(function (result) {
            if (result.status !== 'SUCCESS') {
                $scope.error = status;
            }
            else {
                // Attach the position info to the data...
                result.positions = positionMap;
                $scope.data = result;
            }
        }).error(function (data, status) {
            $scope.error = status;
        });
    };

    $scope.updateGraphs = function () {
        // Invoke $watch on $scope.data...
        $scope.data = {
            status: $scope.data.status,
            bars: $scope.data.bars,
            tradeHours: $scope.data.tradeHours,
            positions: $scope.data.positions
        };
    };

    $scope.resetGraph = function (graph) {
        graph.yOffset = 0;
        graph.yZoom = 0;
        graph.zoomInQueue = [];
        graph.zoomOutQueue = [];
        $scope.updateGraphs();
    };

    $scope.shiftGraph = function (graph, sign) {
        let inc = graph.yRange / 5;
        if (inc < 0.03) {
            inc = 0.01;
        }
        else if (inc < 0.05) {
            inc = 0.02;
        }
        else if (inc < 0.3) {
            inc = 0.1;
        }
        else if (inc < 0.5) {
            inc = 0.2;
        }
        else if (inc < 3) {
            inc = 1;
        }
        else if (inc < 5) {
            inc = 2;
        }
        else {
            inc = 10;
        }

        graph.yOffset += sign * inc;
        graph.yOffset = Math.round(graph.yOffset * 1000) / 1000;
        $scope.updateGraphs();
    };

    $scope.zoomGraph = function (graph, sign) {
        if (sign > 0) {
            if (graph.zoomOutQueue.length > 0) {
                graph.yZoom += graph.zoomOutQueue.pop();
                $scope.updateGraphs();
                return;
            }
        }
        else {
            if (graph.zoomInQueue.length > 0) {
                graph.yZoom -= graph.zoomInQueue.pop();
                $scope.updateGraphs();
                return;
            }
        }

        let inc = graph.yRange / 5;
        if (inc < 0.03) {
            inc = 0.01;
        }
        else if (inc < 0.05) {
            inc = 0.02;
        }
        else if (inc < 0.3) {
            inc = 0.1;
        }
        else if (inc < 0.5) {
            inc = 0.2;
        }
        else if (inc < 3) {
            inc = 1;
        }
        else if (inc < 5) {
            inc = 2;
        }
        else {
            inc = 10;
        }

        graph.yZoom += sign * inc;
        graph.yZoom = Math.round(graph.yZoom * 1000) / 1000;
        if (sign > 0) {
            graph.zoomInQueue.push(inc);
        }
        else {
            graph.zoomOutQueue.push(inc);
        }
        $scope.updateGraphs();
    };

    $scope.graphs = [ {
        title: 'Mid Price',
        key: 'mid',
        isOpen: true,
        yOffset: 0,
        yZoom: 0,
        yRange: 0,
        zoomInQueue: [],
        zoomOutQueue: []
    }, {
        title: 'Spread',
        key: 'spread',
        isOpen: true,
        yOffset: 0,
        yZoom: 0,
        yRange: 0,
        zoomInQueue: [],
        zoomOutQueue: []
    } ];

    $scope.globalSettings = {
        undelier: '',
        extendedHours: false,
        period: '10 days'
    };
} ]);

app.directive('timeSeriesChart', [ 'util', 'timestamp', 'marketTimer', function (util, timestamp, marketTimer) {

    let margin = {
        top: 20,
        right: 20,
        bottom: 45,
        left: 80
    };

    return {
        restrict: 'EA',
        scope: {
            data: '=',
            settings: '=',
            globalSettings: '='
        },
        link: function (scope, element, attrs) {
            scope.$watch('data', function (newVal, oldVal) {
                let settings = scope.settings;
                let globalSettings = scope.globalSettings;

                if (!settings.isOpen) {
                    return;
                }

                let data = newVal;

                // If there is no data, exit.
                if (!data) {
                    return;
                }

                margin.left = 70;
                let w0 = 800; // Width of the drawing area
                let h0 = 300; // Height of the drawing area
                let w = w0 - margin.left - margin.right; // Width of graph area
                let h = h0 - margin.top - margin.bottom; // Height of graph area

                // Clear all the elements previously drawn
                d3.select(element[ 0 ]).selectAll('*').remove();

                // Define top-level SVG object
                let svg = d3.select(element[ 0 ]).append('svg');

                svg.attr("preserveAspectRatio", "xMinYMin meet").attr('viewBox', '0,0,' + w0 + ',' + h0);
                let g = svg.append('g').attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

                // Define the clipping region
                let rect = g.append('clipPath').attr('id', 'contents-area').append('rect');
                // Slightly Extend the clip region horizontally so the points on the edge are not truncated.
                rect.attr('width', w + 20).attr('height', h).attr('x', -10);
                // rect.attr('stroke', '#777777').attr('stroke-width', 2).attr('fill', '#eeeeee');

                let extendedHours = globalSettings.extendedHours;

                let targetData = {};

                let maxY;
                let minY;

                for (let symbol in data.bars) {
                    let array = data.bars[ symbol ];
                    let arrayLen = array.length;

                    if (arrayLen === 0) {
                        continue;
                    }

                    for (let i = 0; i < arrayLen; ++i) {
                        let bar = array[ i ];

                        if (!extendedHours && !timestamp.isMarketOpen(bar.timestamp)) {
                            continue;
                        }

                        bar.time = timestamp.toDate(bar.timestamp);

                        if (settings.key === 'mid') {
                            bar.y = bar.close;
                        }
                        else if (settings.key === 'spread') {
                            if (bar.spreadHigh - bar.spreadLow > bar.spreadLow) {
                                // Price is usually unstable at the market open.
                                // We don't want to take spreadHigh there...
                                bar.y = bar.spreadLow;
                            }
                            else {
                                bar.y = bar.spreadHigh;
                            }
                        }

                        // ETFs occasionally have a huge drop, which appears as a bar's low.
                        // Since we only show close values on a graph, we only use close values...

                        if (!maxY || (maxY < bar.y)) {
                            maxY = bar.y;
                        }
                        if (!minY || (bar.y < minY)) {
                            minY = bar.y;
                        }
                    }

                    targetData[ symbol ] = {
                        data: array,
                        color: util.parseOptionSymbol(symbol).optionType === 'C' ? 200 : 207
                    };

                    let positions = data.positions[ symbol ];
                    if (positions) {
                        targetData[ symbol ].positions = positions;
                    }
                }

                if ((Math.abs(maxY) > 1) || (Math.abs(minY) > 1)) {
                    maxY = Math.ceil(maxY);
                    minY = Math.floor(minY);
                }
                else {
                    maxY = Math.ceil(maxY * 10) / 10;
                    minY = Math.floor(minY * 10) / 10;
                }

                settings.yRange = maxY - minY;

                maxY = Math.round(maxY * 1000) / 1000;
                minY = Math.round(minY * 1000) / 1000;
                settings.yRange = Math.round(settings.yRange * 1000) / 1000;

                // X-axis
                marketTimer.init(data.tradeHours, extendedHours);

                let start = data.tradeHours[ 0 ].extStart;
                let end = data.tradeHours[ data.tradeHours.length - 1 ].extEnd;

                if (!extendedHours) {
                    start = timestamp.snapToMarketOpen(start);
                    end = timestamp.snapToMarketClose(end);
                }

                let xTicks = marketTimer.generateTicks();
                let xScale = d3.scale.linear();
                xScale.domain([ marketTimer.get(start), marketTimer.get(end) ]).range([ 0, w ]);
                let xAxis = d3.svg.axis().scale(xScale).orient('bottom');

                if (extendedHours) {
                    // Show gray zones to indicate extended hour trading
                    let zones = marketTimer.getExtendedHourZones();
                    zones.forEach(function (zone) {
                        let start = marketTimer.get(zone.start);
                        let end = marketTimer.get(zone.end);

                        if ((start < 0) || (end < 0)) {
                            return;
                        }

                        let s = xScale(start);
                        let e = xScale(end);

                        let z = g.append('rect').attr('x', s).attr('y', 0).attr('width', (e - s)).attr('height', h);
                        z.attr('stroke', '#eeeeee').attr('stroke-width', 1).attr('fill', '#eeeeee');
                        z.attr('clip-path', 'url(#contents-area)');
                    });
                }

                xAxis.tickValues(xTicks).tickFormat(function (val) {
                    return marketTimer.tickFormat(val);
                }).tickSize(5, 0.5);

                let xg = g.append('g').attr('class', 'x-axis').attr('transform', 'translate(0,' + h + ')').call(xAxis);

                let xLabels = xg.selectAll('text').attr('fill', '#777777').attr("stroke-width", 0);
                xLabels.style('font-size', '10px');
                xLabels.style('text-anchor', 'start').attr('dx', '.8em').attr('dy', '-.3em');
                xLabels.attr('transform', function (d) {
                    return 'rotate(+90)';
                });

                xg.selectAll('path,line').attr('fill', '#777777').attr('stroke', '#777777').attr("stroke-width", 1);

                let xGrids = d3.selectAll("g.x-axis g.tick").append("line").classed("grid-line", true);
                xGrids.attr('fill', 'none').attr('stroke', '#777777');
                xGrids.attr("x1", 0).attr("y1", 0).attr("x2", 0).attr("y2", -h);

                // Y-axis
                let yTicks = [];
                let delta;
                let fullY = maxY - minY;
                if (fullY <= 1) {
                    delta = 0.1;
                }
                else if (fullY <= 10) {
                    delta = 1;
                }
                else if (fullY <= 20) {
                    delta = 2;
                }
                else if (fullY <= 100) {
                    delta = 10;
                }
                else if (fullY <= 200) {
                    delta = 20;
                }
                else if (fullY <= 1000) {
                    delta = 50;
                }
                else if (fullY <= 2000) {
                    delta = 100;
                }
                else if (fullY <= 5000) {
                    delta = 250;
                }
                else {
                    delta = 500;
                }

                if (minY < delta) {
                    minY = 0;
                }

                let yScale = d3.scale.linear().domain([ maxY, minY ]).range([ 0, h ]);
                let yAxis = d3.svg.axis().scale(yScale).orient('left').tickSize(5, 0.5);

                for (let i = minY; i < maxY + delta / 10; i += delta) {
                    i = Math.round(i * 100) / 100;
                    yTicks.push(i);
                }
                yAxis.tickValues(yTicks).tickFormat(function (val) {
                    return val / 100;
                }).tickSize(5, 0.5);

                let yg = g.append('g').attr('class', 'y-axis').call(yAxis);
                let yLabels = yg.selectAll('text');
                yLabels.style('font-size', '10px');
                yLabels.attr('fill', '#777777').attr("stroke-width", 0).style('text-anchor', 'end');

                yg.selectAll('path,line').attr('fill', 'none').attr('stroke', '#777777');

                let yGrids = d3.selectAll("g.y-axis g.tick").append("line").classed("grid-line", true);
                yGrids.attr('fill', 'none').attr('stroke', '#777777');
                yGrids.attr("x1", 0).attr("y1", 0).attr("x2", w).attr("y2", 0);

                let plot = d3.svg.line().x(function (d) {
                    return xScale(marketTimer.get(d.timestamp));
                }).y(function (d) {
                    return yScale(d.y);
                });

                for (let key in targetData) {
                    let item = targetData[ key ];

                    let plotData = item.data;
                    if (!extendedHours) {
                        plotData = [];
                        item.data.forEach(function (p) {
                            if (timestamp.isMarketOpen(p.timestamp)) {
                                plotData.push(p);
                            }
                        });
                    }

                    let color = util.getColor(item.color);
                    let seriesLine = g.append('path').attr('class', 'line').attr('d', plot(plotData));
                    seriesLine.attr('stroke', color).attr('stroke-width', 1.2).attr('fill', 'none');
                    seriesLine.attr('clip-path', 'url(#contents-area)');

                    if (settings.key === 'mid') {
                        if (item.positions) {
                            let posLen = item.positions.length;
                            for (let i = 0; i < posLen; ++i) {
                                let pos = item.positions[ i ];

                                // Closing position has quantity = 0, which makes perSharePrice NaN
                                let perSharePrice = Math.abs(pos.amount / pos.quantity);
                                let x = 0;
                                let y = 0;

                                // Unfortunately, Fidelity doesn't tell the exact time when the position is opened/closed.
                                // We just find the bar that matches the price for the first time on the day, and adjust time...
                                let len = plotData.length;
                                for (let j = 0; j < len; ++j) {
                                    let p = plotData[ j ];
                                    if (timestamp.getDatePart(pos.date) !== timestamp.getDatePart(p.timestamp)) {
                                        continue;
                                    }

                                    if ((isNaN(perSharePrice) || ((p.low <= perSharePrice) && (perSharePrice <= p.high)))) {
                                        x = timestamp.replaceTimePart(pos.date, timestamp.getTimePart(p.timestamp));
                                        y = p.y;
                                        break;
                                    }
                                }

                                // Status: 10 --> open, 30 --> close.
                                var point;
                                if (pos.status === 10) {
                                    point = g.append('circle').attr('r', 4);
                                    point.attr('cx', xScale(marketTimer.get(x))).attr('cy', yScale(y));
                                }
                                else {
                                    point = g.append('rect').attr('width', 8).attr('height', 8);
                                    point.attr('x', xScale(marketTimer.get(x)) - 4).attr('y', yScale(y) - 4);
                                }
                                point.style('stroke', color).attr('stroke-width', 3).style('fill', '#eeeeee');
                                point.attr('clip-path', 'url(#contents-area)');
                            }
                        }
                    }
                }
            });
        }
    };
} ]);

app.directive('tsLegend', [ 'util', function (util) {
    return {
        restrict: 'EA',
        scope: {
            color: '='
        },
        link: function (scope, element, attrs) {
            let color = scope.color;

            // Clear all the elements previously drawn
            d3.select(element[ 0 ]).selectAll('*').remove();

            // Define top-level SVG object
            let svg = d3.select(element[ 0 ]).append('svg');

            let width = 25; // Width of the drawing area
            let height = 26; // Height of the drawing area
            let hOffset = 10;

            svg.attr('width', width).attr('height', height).attr('viewBox', '0,0,' + width + ',' + height);
            let g = svg.append('g');

            let func = d3.svg.line().x(function (d) {
                return d.x;
            }).y(function (d) {
                return d.y;
            });

            let p0 = {
                x: 0,
                y: height / 2 + hOffset
            };

            let p1 = {
                x: width * 0.7,
                y: height / 2 + hOffset
            };

            let line = g.append('path').attr('d', func([ p0, p1 ]));
            line.style('stroke', util.getColor(color));
            line.attr('stroke-width', 4).attr('fill', '#ffffff');

            // let point = g.append('circle').attr('r', 4);
            // point.attr('cx', width / 2).attr('cy', height / 2 + hOffset);
            // point.style('stroke', util.getColor(color));
            // point.attr('stroke-width', 1).attr('fill', '#ffffff');
        }
    };
} ]);