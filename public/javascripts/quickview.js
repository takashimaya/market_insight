'use strict';

/**
 * The app
 */
let app = angular.module('app', [ 'ui.bootstrap', 'util', 'timestamp', 'marketTimer' ]);

/**
 * The controller for Balances page.
 */
app.controller('QuickViewCtrl', [ '$scope', '$location', '$http', 'util', 'timestamp', function ($scope, $location, $http, util, timestamp) {
    $scope.theme = $location.absUrl();
    $scope.theme = $scope.theme.substr($scope.theme.lastIndexOf('/') + 1);

    $scope.globalSettings = {
        extendedHours: true,
        period: '10 days'
    };

    if ($scope.theme === 'equities') {
        $scope.graphs = [ {
            title: 'Index ETFs',
            isOpen: true,
            targets: [ {
                name: 'S&P',
                key: 'SPY',
                visible: true,
                color: 200,
                core: true
            }, {
                name: 'Nasdaq',
                key: 'QQQ',
                visible: true,
                color: 203,
                core: true
            }, {
                name: 'Russel 2000',
                key: 'IWM',
                visible: true,
                color: 204,
                core: true
            }, {
                name: 'Dow Jones',
                key: 'DIA',
                visible: true,
                color: 205,
                core: true
            } ],
            refDate: null,
            yOffset: 0,
            yZoom: 0,
            yRange: 0,
            zoomInQueue: [],
            zoomOutQueue: []
        }, {
            title: 'Sector ETFs',
            isOpen: true,
            targets: [ {
                name: 'Energy (XLE)',
                key: 'XLE',
                visible: true,
                color: 200,
                core: true
            }, {
                name: 'Oil&Gas (XOP)',
                key: 'XOP',
                visible: false,
                color: 201,
                core: false
            }, {
                name: 'Finance (XLF)',
                key: 'XLF',
                visible: true,
                color: 202,
                core: true
            }, {
                name: 'Reg Banks (KRE)',
                key: 'KRE',
                visible: false,
                color: 214,
                core: false
            }, {
                name: 'Tech (XLK)',
                key: 'XLK',
                visible: true,
                color: 203,
                core: true
            }, {
                name: 'Semi (SOXX)',
                key: 'SOXX',
                visible: false,
                color: 212,
                core: false
            }, {
                name: 'Health (XLV)',
                key: 'XLV',
                visible: true,
                color: 204,
                core: true
            }, {
                name: 'Bio (IBB)',
                key: 'IBB',
                visible: false,
                color: 219,
                core: false
            }, {
                name: 'Real Estate (IYR)',
                key: 'IYR',
                visible: true,
                color: 205,
                core: true
            }, {
                name: 'Home Build (ITB)',
                key: 'ITB',
                visible: false,
                color: 220,
                core: false
            }, {
                name: 'Industrial (XLI)',
                key: 'XLI',
                visible: true,
                color: 206,
                core: true
            }, {
                name: 'Materials (XLB)',
                key: 'XLB',
                visible: true,
                color: 207,
                core: true
            }, {
                name: 'Mining (XME)',
                key: 'XME',
                visible: false,
                color: 215,
                core: false
            }, {
                name: 'Cons Disc (KLY)',
                key: 'XLY',
                visible: false,
                color: 211,
                core: true
            }, {
                name: 'Cons Stpl (XLP)',
                key: 'XLP',
                visible: false,
                color: 222,
                core: true
            }, {
                name: 'Retail (XRT)',
                key: 'XRT',
                visible: false,
                color: 209,
                core: false
            }, {
                name: 'Util (XLU)',
                key: 'XLU',
                visible: false,
                color: 223,
                core: true
            } ],
            refDate: null,
            yOffset: 0,
            yZoom: 0,
            yRange: 0,
            zoomInQueue: [],
            zoomOutQueue: []
        } ];
    }
    else if ($scope.theme === 'countries') {
        $scope.graphs = [ {
            title: 'Region ETFs',
            isOpen: true,
            targets: [ {
                name: 'Japan (EWJ)',
                key: 'EWJ',
                visible: true,
                color: 200,
                core: true
            }, {
                name: 'Brazil (EWZ)',
                key: 'EWZ',
                visible: true,
                color: 219,
                core: true
            }, {
                name: 'Canada (EWC)',
                key: 'EWC',
                visible: false,
                color: 207,
                core: true
            }, {
                name: 'Mexico (EWW)',
                key: 'EWW',
                visible: true,
                color: 215,
                core: true
            }, {
                name: 'UK (EWU)',
                key: 'EWU',
                visible: true,
                color: 211,
                core: true
            }, {
                name: 'Germany (EWG)',
                key: 'EWG',
                visible: true,
                color: 205,
                core: true
            }, {
                name: 'Italy (EWI)',
                key: 'EWI',
                visible: false,
                color: 220,
                core: true
            }, {
                name: 'Spain (EWP)',
                key: 'EWP',
                visible: false,
                color: 221,
                core: true
            } ],
            refDate: null,
            yOffset: 0,
            yZoom: 0,
            yRange: 0,
            zoomInQueue: [],
            zoomOutQueue: []
        }, {
            title: 'Currencies',
            isOpen: true,
            targets: [ {
                name: 'JPY',
                key: 'JPY_0',
                visible: true,
                color: 200,
                core: true
            }, {
                name: 'EUR',
                key: 'EUR_0',
                visible: true,
                color: 205,
                core: true
            }, {
                name: 'CHF',
                key: 'CHF_0',
                visible: true,
                color: 221,
                core: true
            }, {
                name: 'GBP',
                key: 'GBP_0',
                visible: true,
                color: 211,
                core: true
            } ],
            refDate: null,
            yOffset: 0,
            yZoom: 0,
            yRange: 0,
            zoomInQueue: [],
            zoomOutQueue: []
        } ];
    }
    else if ($scope.theme === 'commodities') {
        $scope.graphs = [ {
            title: 'Energy',
            isOpen: true,
            targets: [ {
                name: 'CL 1m',
                key: 'CL_1',
                visible: true,
                color: 200,
                core: true
            }, {
                name: 'CL 6m',
                key: 'CL_6',
                visible: true,
                color: 217,
                core: true
            }, {
                name: 'CL 1y',
                key: 'CL_12',
                visible: true,
                color: 210,
                core: true
            } ],
            refDate: null,
            yOffset: 0,
            yZoom: 0,
            yRange: 0
        }, {
            title: 'Metals',
            isOpen: true,
            targets: [ {
                name: 'GC',
                key: 'GC_2',
                visible: true,
                color: 204,
                core: true
            }, {
                name: 'SI',
                key: 'SI_0',
                visible: true,
                color: 200,
                core: true
            }, {
                name: 'HG',
                key: 'HG_0',
                visible: true,
                color: 220,
                core: true
            } ],
            refDate: null,
            yOffset: 0,
            yZoom: 0,
            yRange: 0,
            zoomInQueue: [],
            zoomOutQueue: []
        } ];
    }
    else if ($scope.theme === 'fixedincome') {
        $scope.graphs = [ {
            title: 'Eurodollar',
            isOpen: true,
            targets: [ {
                name: 'Eurodollar 1 mon',
                key: 'GE_1',
                visible: true,
                color: 200,
                core: true
            }, {
                name: 'Eurodollar 6 mon',
                key: 'GE_6',
                visible: true,
                color: 217,
                core: true
            }, {
                name: 'Eurodollar 1 yr',
                key: 'GE_12',
                visible: true,
                color: 210,
                core: true
            }, {
                name: 'Eurodollar 3 yr',
                key: 'GE_36',
                visible: true,
                color: 224,
                core: true
            }, {
                name: 'Eurodollar 6 yr',
                key: 'GE_72',
                visible: true,
                color: 212,
                core: true
            } ],
            refDate: null,
            yOffset: 0,
            yZoom: 0,
            yRange: 0,
            commonRef: false,
            zoomInQueue: [],
            zoomOutQueue: []
        }, {
            title: 'T-Bills/Notes/Bonds',
            isOpen: true,
            targets: [ {
                name: 'Treasury 2 yr',
                key: 'ZT_1',
                visible: true,
                color: 200,
                core: true
            }, {
                name: 'Treasury 5 yr',
                key: 'ZF_1',
                visible: true,
                color: 217,
                core: true
            }, {
                name: 'Treasury 10 yr',
                key: 'ZN_1',
                visible: true,
                color: 210,
                core: true
            } ],
            refDate: null,
            yOffset: 0,
            yZoom: 0,
            yRange: 0,
            commonRef: false,
            zoomInQueue: [],
            zoomOutQueue: []
        } ];
    }

    $scope.update = function () {
        let timeParams = timestamp.getServerRequestParams($scope.globalSettings.period);

        let symbols = '';
        $scope.graphs.forEach(function (graph) {
            let len = graph.targets.length;
            for (let i = 0; i < len; ++i) {
                // If there is a case where the same symbol appears more than once, we should check if the key has already been added.
                symbols += graph.targets[ i ].key + ',';
            }
        });
        symbols.slice(0, -1);

        let config = {
            method: 'GET',
            url: 'http://localhost:3300/bars',
            params: {
                symbols: symbols,
                start: timeParams.start,
                end: timeParams.end,
                resolution: timeParams.resolution
            }
        };
        $http(config).success(function (result) {
            if (result.status !== 'SUCCESS') {
                $scope.error = status;
            }
            else {
                $scope.data = result;
            }
        }).error(function (data, status) {
            $scope.error = status;
        });
    };

    $scope.update();

    $scope.updateGraphs = function () {
        // Invoke $watch on $scope.data...
        $scope.data = {
            status: $scope.data.status,
            bars: $scope.data.bars,
            tradeHours: $scope.data.tradeHours
        };
    };

    $scope.resetGraph = function (graph) {
        graph.yOffset = 0;
        graph.yZoom = 0;
        graph.zoomInQueue = [];
        graph.zoomOutQueue = [];
        $scope.updateGraphs();
    };

    $scope.shiftGraph = function (graph, sign) {
        let inc = graph.yRange / 5;
        if (inc < 0.03) {
            inc = 0.01;
        }
        else if (inc < 0.05) {
            inc = 0.02;
        }
        else if (inc < 0.3) {
            inc = 0.1;
        }
        else if (inc < 0.5) {
            inc = 0.2;
        }
        else if (inc < 3) {
            inc = 1;
        }
        else if (inc < 5) {
            inc = 2;
        }
        else {
            inc = 10;
        }

        graph.yOffset += sign * inc;
        graph.yOffset = Math.round(graph.yOffset * 1000) / 1000;
        $scope.updateGraphs();
    };

    $scope.zoomGraph = function (graph, sign) {
        if (sign > 0) {
            if (graph.zoomOutQueue.length > 0) {
                graph.yZoom += graph.zoomOutQueue.pop();
                $scope.updateGraphs();
                return;
            }
        }
        else {
            if (graph.zoomInQueue.length > 0) {
                graph.yZoom -= graph.zoomInQueue.pop();
                $scope.updateGraphs();
                return;
            }
        }

        let inc = graph.yRange / 5;
        if (inc < 0.03) {
            inc = 0.01;
        }
        else if (inc < 0.05) {
            inc = 0.02;
        }
        else if (inc < 0.3) {
            inc = 0.1;
        }
        else if (inc < 0.5) {
            inc = 0.2;
        }
        else if (inc < 3) {
            inc = 1;
        }
        else if (inc < 5) {
            inc = 2;
        }
        else {
            inc = 10;
        }

        graph.yZoom += sign * inc;
        graph.yZoom = Math.round(graph.yZoom * 1000) / 1000;
        if (sign > 0) {
            graph.zoomInQueue.push(inc);
        }
        else {
            graph.zoomOutQueue.push(inc);
        }
        $scope.updateGraphs();
    };
} ]);

app.directive('timeSeriesChart', [ 'util', 'timestamp', 'marketTimer', function (util, timestamp, marketTimer) {

    let margin = {
        top: 20,
        right: 20,
        bottom: 45,
        left: 20
    };

    return {
        restrict: 'EA',
        scope: {
            data: '=',
            settings: '=',
            globalSettings: '='
        },
        link: function (scope, element, attrs) {
            scope.$watch('data', function (newVal, oldVal) {
                let settings = scope.settings;
                let globalSettings = scope.globalSettings;

                if (!settings.isOpen) {
                    return;
                }

                let data = newVal;

                // If there is no data, exit.
                if (!data) {
                    return;
                }

                margin.left = 70;
                let w0 = 800; // Width of the drawing area
                let h0 = 300; // Height of the drawing area
                let w = w0 - margin.left - margin.right; // Width of graph area
                let h = h0 - margin.top - margin.bottom; // Height of graph area

                // Clear all the elements previously drawn
                d3.select(element[ 0 ]).selectAll('*').remove();

                // Define top-level SVG object
                let svg = d3.select(element[ 0 ]).append('svg');

                svg.attr("preserveAspectRatio", "xMinYMin meet").attr('viewBox', '0,0,' + w0 + ',' + h0);
                let g = svg.append('g').attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

                // Define the clipping region
                let rect = g.append('clipPath').attr('id', 'contents-area').append('rect');
                // Slightly Extend the clip region horizontally so the points on the edge are not truncated.
                rect.attr('width', w + 20).attr('height', h).attr('x', -10);
                // rect.attr('stroke', '#777777').attr('stroke-width', 2).attr('fill', '#eeeeee');

                let extendedHours = globalSettings.extendedHours;

                let targetData = {};
                let maxYPos;
                let maxYNeg;

                let commonRefY = null;

                for (let symbol in data.bars) {
                    let target = null;
                    let targetLen = settings.targets.length;
                    for (let i = 0; i < targetLen; ++i) {
                        if ((settings.targets[ i ].key === symbol) && settings.targets[ i ].visible) {
                            target = settings.targets[ i ];
                            break;
                        }
                    }

                    if (!target) {
                        continue;
                    }

                    let array = data.bars[ symbol ];
                    let arrayLen = array.length;

                    if (arrayLen === 0) {
                        continue;
                    }

                    let refY = null;
                    if (settings.commonRef) {
                        refY = commonRefY;
                    }

                    let maxY = undefined;
                    let minY = undefined;
                    for (let i = 0; i < arrayLen; ++i) {
                        let bar = array[ i ];

                        if (!extendedHours && !timestamp.isMarketOpen(bar.timestamp)) {
                            continue;
                        }

                        bar.time = timestamp.toDate(bar.timestamp);

                        // ETFs occasionally have a huge drop, which appears as a bar's low.
                        // Since we only show close values on a graph, we only use close values...

                        if (!commonRefY) {
                            commonRefY = bar.close;
                        }

                        if (!refY) {
                            refY = bar.close;
                        }

                        if (target.refDate && (bar.time.getTime() <= target.refDate.getTime())) {
                            refY = bar.close;
                        }

                        if (!maxY || (maxY < bar.close)) {
                            maxY = bar.close;
                        }
                        if (!minY || (bar.close < minY)) {
                            minY = bar.close;
                        }
                    }

                    targetData[ symbol ] = {
                        maxY: maxY,
                        minY: minY,
                        refY: refY,
                        data: array,
                        color: target.color
                    };

                    let yPos = 0;
                    let yNeg = 0;
                    if (refY) {
                        yPos = (maxY - refY) * 100 / refY;
                        yNeg = (refY - minY) * 100 / refY;
                    }
                    if (!maxYPos || (maxYPos < yPos)) {
                        maxYPos = yPos;
                    }
                    if (!maxYNeg || (maxYNeg < yNeg)) {
                        maxYNeg = yNeg;
                    }
                }

                if ((maxYPos > 1) || (maxYNeg > 1)) {
                    maxYPos = Math.ceil(maxYPos);
                    maxYNeg = Math.ceil(maxYNeg);
                }
                else {
                    maxYPos = Math.ceil(maxYPos * 10) / 10;
                    maxYNeg = Math.ceil(maxYNeg * 10) / 10;
                }

                maxYPos += settings.yOffset;
                maxYNeg -= settings.yOffset;

                maxYPos -= settings.yZoom;
                maxYNeg -= settings.yZoom;

                settings.yRange = maxYPos + maxYNeg;

                maxYPos = Math.round(maxYPos * 1000) / 1000;
                maxYNeg = Math.round(maxYNeg * 1000) / 1000;
                settings.yRange = Math.round(settings.yRange * 1000) / 1000;

                // X-axis
                marketTimer.init(data.tradeHours, extendedHours);

                let start = data.tradeHours[ 0 ].extStart;
                let end = data.tradeHours[ data.tradeHours.length - 1 ].extEnd;

                if (!extendedHours) {
                    start = timestamp.snapToMarketOpen(start);
                    end = timestamp.snapToMarketClose(end);
                }

                let xTicks = marketTimer.generateTicks();
                let xScale = d3.scale.linear();
                xScale.domain([ marketTimer.get(start), marketTimer.get(end) ]).range([ 0, w ]);
                let xAxis = d3.svg.axis().scale(xScale).orient('bottom');

                if (extendedHours) {
                    // Show gray zones to indicate extended hour trading
                    let zones = marketTimer.getExtendedHourZones();
                    zones.forEach(function (zone) {
                        let start = marketTimer.get(zone.start);
                        let end = marketTimer.get(zone.end);

                        if ((start < 0) || (end < 0)) {
                            return;
                        }

                        let s = xScale(start);
                        let e = xScale(end);

                        let z = g.append('rect').attr('x', s).attr('y', 0).attr('width', (e - s)).attr('height', h);
                        z.attr('stroke', '#eeeeee').attr('stroke-width', 1).attr('fill', '#eeeeee');
                        z.attr('clip-path', 'url(#contents-area)');
                    });
                }

                xAxis.tickValues(xTicks).tickFormat(function (val) {
                    return marketTimer.tickFormat(val);
                }).tickSize(5, 0.5);

                let xg = g.append('g').attr('class', 'x-axis').attr('transform', 'translate(0,' + h + ')').call(xAxis);

                let xLabels = xg.selectAll('text').attr('fill', '#777777').attr("stroke-width", 0);
                xLabels.style('font-size', '10px');
                xLabels.style('text-anchor', 'start').attr('dx', '.8em').attr('dy', '-.3em');
                xLabels.attr('transform', function (d) {
                    return 'rotate(+90)';
                });

                xg.selectAll('path,line').attr('fill', '#777777').attr('stroke', '#777777').attr("stroke-width", 1);

                let xGrids = d3.selectAll("g.x-axis g.tick").append("line").classed("grid-line", true);
                xGrids.attr('fill', 'none').attr('stroke', '#777777');
                xGrids.attr("x1", 0).attr("y1", 0).attr("x2", 0).attr("y2", -h);

                // Y-axis
                let yScale = d3.scale.linear().domain([ maxYPos, (-1) * maxYNeg ]).range([ 0, h ]);
                let yAxis = d3.svg.axis().scale(yScale).orient('left').tickSize(5, 0.5);

                let yTicks = [];
                let delta;
                let fullY = maxYPos + maxYNeg;
                if (fullY <= 1) {
                    delta = 0.1;
                }
                else if (fullY <= 10) {
                    delta = 1;
                }
                else if (fullY <= 20) {
                    delta = 2;
                }
                else {
                    delta = 5;
                }

                for (let i = (-1) * maxYNeg; i < maxYPos + delta / 10; i += delta) {
                    i = Math.round(i * 100) / 100;
                    yTicks.push(i);
                }
                yAxis.tickValues(yTicks).tickFormat(function (val) {
                    return val;
                }).tickSize(5, 0.5);

                let yg = g.append('g').attr('class', 'y-axis').call(yAxis);
                let yLabels = yg.selectAll('text');
                yLabels.style('font-size', '10px');
                yLabels.attr('fill', '#777777').attr("stroke-width", 0).style('text-anchor', 'end');

                yg.selectAll('path,line').attr('fill', 'none').attr('stroke', '#777777');

                let yGrids = d3.selectAll("g.y-axis g.tick").append("line").classed("grid-line", true);
                yGrids.attr('fill', 'none').attr('stroke', '#777777');
                yGrids.attr("x1", 0).attr("y1", 0).attr("x2", w).attr("y2", 0);

                for (let key in targetData) {
                    let item = targetData[ key ];

                    let plot = d3.svg.line().x(function (d) {
                        return xScale(marketTimer.get(d.timestamp));
                    }).y(function (d) {
                        return yScale((d.close - item.refY) * 100 / item.refY);
                    });

                    let plotData = item.data;
                    if (!extendedHours) {
                        plotData = [];
                        item.data.forEach(function (p) {
                            if (timestamp.isMarketOpen(p.timestamp)) {
                                plotData.push(p);
                            }
                        });
                    }

                    let color = util.getColor(item.color);
                    let seriesLine = g.append('path').attr('class', 'line').attr('d', plot(plotData));
                    seriesLine.attr('stroke', color).attr('stroke-width', 1.2).attr('fill', 'none');
                    seriesLine.attr('clip-path', 'url(#contents-area)');
                }
            });
        }
    };
} ]);

app.directive('tsLegend', [ 'util', function (util) {
    return {
        restrict: 'EA',
        scope: {
            color: '='
        },
        link: function (scope, element, attrs) {
            let color = scope.color;

            // Clear all the elements previously drawn
            d3.select(element[ 0 ]).selectAll('*').remove();

            // Define top-level SVG object
            let svg = d3.select(element[ 0 ]).append('svg');

            let width = 25; // Width of the drawing area
            let height = 26; // Height of the drawing area
            let hOffset = 10;

            svg.attr('width', width).attr('height', height).attr('viewBox', '0,0,' + width + ',' + height);
            let g = svg.append('g');

            let func = d3.svg.line().x(function (d) {
                return d.x;
            }).y(function (d) {
                return d.y;
            });

            let p0 = {
                x: 0,
                y: height / 2 + hOffset
            };

            let p1 = {
                x: width * 0.7,
                y: height / 2 + hOffset
            };

            let line = g.append('path').attr('d', func([ p0, p1 ]));
            line.style('stroke', util.getColor(color));
            line.attr('stroke-width', 4).attr('fill', '#ffffff');

            // let point = g.append('circle').attr('r', 4);
            // point.attr('cx', width / 2).attr('cy', height / 2 + hOffset);
            // point.style('stroke', util.getColor(color));
            // point.attr('stroke-width', 1).attr('fill', '#ffffff');
        }
    };
} ]);