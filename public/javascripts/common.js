'use strict';

/**
 * This filter takes an integer corresponding to Date.getTime() and returns a Date object.
 */
angular.module('clockToDate', []).filter('ctd', function () {
    return function (input) {
        if (!input) {
            return '-';
        }
        return new Date(input);
    };
});

/**
 * This filter takes an timestamp and returns a date string.
 */
angular.module('timestampToDate', [ 'timestamp' ]).filter('ttd', function (timestamp) {
    return function (input) {
        if (!input) {
            return '';
        }

        return timestamp.getDatePart(input);
    };
});

angular.module('serverLog', []).factory('serverLog', function () {
    let socket = io('http://localhost:3300');

    return {
        start: function (callback) {
            socket.on('log', function (contents) {
                callback(contents);
            });
            socket.emit('start', {});
        },
        stop: function () {
            socket.emit('stop', {});
        }
    };
});

angular.module('timestamp', [ 'util' ]).factory('timestamp', [ 'util', function (util) {
    let fromDate_ = function (date) {
        let temp = new Date(date);
        // temp.setHours(temp.getHours() + 3); // Convert to EST/EDT
        let y = temp.getFullYear().toString();
        let m = util.padInt(temp.getMonth() + 1, 2);
        let d = util.padInt(temp.getDate(), 2);
        let h = util.padInt(temp.getHours(), 2);
        let min = util.padInt(temp.getMinutes(), 2);
        let s = util.padInt(temp.getSeconds(), 2);
        return y + m + d + h + min + s;
    };

    let toDate_ = function (t) {
        t = t.toString();
        let y = t.substring(0, 4);
        let m = t.substring(4, 6);
        let d = t.substring(6, 8);

        if (t.length === 8) {
            return new Date(y, m - 1, d);
        }

        let h = t.substring(8, 10);
        let min = t.substring(10, 12);
        let s = t.substring(12, 14);

        let temp = new Date(y, m - 1, d, h, min, s);
        // temp.setHours(temp.getHours() - 3); // Convert EST/EDT to PST/PDT
        return temp;
    };

    let shiftDate_ = function (date, numOfDays) {
        date.setHours(0, 0, 0, 0);
        let i = numOfDays;
        while (i > 0) {
            date.setDate(date.getDate() - 1);
            let day = date.getDay();
            if ((1 <= day) && (day <= 5)) {
                --i;
            }
        }
    };

    let getDatePart_ = function (t, preserveDigits) {
        if (preserveDigits) {
            return Math.floor(t / 1000000) * 1000000;
        }
        else {
            return Math.floor(t / 1000000);
        }
    };

    return {
        fromDate: function (date) {
            return fromDate_(date);
        },

        toDate: function (t) {
            return toDate_(t);
        },

        snapToMarketOpen: function (t) {
            return Math.round(t / 1000000) * 1000000 + 93000;
        },

        snapToMarketClose: function (t) {
            return Math.round(t / 1000000) * 1000000 + 160000;
        },

        isMarketOpen: function (t) {
            let d = toDate_(t);
            if ((d.getDay() === 0) || (d.getDay() === 6)) {
                return false;
            }

            if (d.getHours() < 9) {
                return false;
            }

            if ((d.getHours() === 9) && (d.getMinutes() < 30)) {
                return false;
            }

            if (16 < d.getHours()) {
                return false;
            }

            if ((d.getHours() === 16) && (0 < d.getMinutes())) {
                return false;
            }

            return true;
        },

        getDatePart: function (t) {
            return getDatePart_(t, false);
        },

        getTimePart: function (t) {
            return t - getDatePart_(t, true);
        },

        replaceTimePart: function (t, timePart) {
            return getDatePart_(t, true) + timePart;
        },

        getSeconds: function (a, b) {
            return Math.round((toDate_(a).getTime() - toDate_(b).getTime()) / 1000);
        },

        getServerRequestParams: function (period) {
            let startTimestamp;
            let endTimestamp;
            let resolution;

            let startDate;
            let endDate;

            let date = new Date();
            if (date.getDay() === 0) {
                date.setDate(date.getDate() - 2);
                date.setHours(20, 59, 59);
            }
            else if (date.getDay() === 6) {
                date.setDate(date.getDate() - 1);
                date.setHours(20, 59, 59);
            }

            endDate = new Date(date);
            endDate.setHours(endDate.getHours() + 3); // Convert PDT/PST to EDT/EST.
            endTimestamp = fromDate_(endDate);

            if (period === '2 days') {
                resolution = '5m';
                shiftDate_(date, 1);
            }
            else if (period === '3 days') {
                resolution = '5m';
                shiftDate_(date, 2);
            }
            else if (period === '5 days') {
                resolution = '5m';
                shiftDate_(date, 4);
            }
            else if (period === '10 days') {
                resolution = '5m';
                shiftDate_(date, 9);
            }
            else if (period === '20 days') {
                resolution = '5m';
                shiftDate_(date, 20);
            }
            else if (period === '1 month') {
                resolution = '30m';
                shiftDate_(date, 30);
            }
            else if (period === '3 months') {
                resolution = '30m';
                shiftDate_(date, 90);
            }
            else if (period === '4 months') {
                resolution = '30m';
                shiftDate_(date, 120);
            }
            else if (period === '5 months') {
                resolution = '30m';
                shiftDate_(date, 150);
            }
            else if (period === '6 months') {
                resolution = '1d';
                shiftDate_(date, 180);
            }
            else if (period === '1 year') {
                resolution = '1d';
                shiftDate_(date, 365);
            }
            else if (period === 'max') {
                resolution = '1d';
                date = new Date(2016, 11, 1, 0, 0, 0, 0);
            }
            else if (period === 'ytd') {
                resolution = '30m';
                date = new Date(date.getFullYear(), 0, 1, 0, 0, 0, 0);
            }

            startDate = new Date(date);
            startDate.setHours(startDate.getHours() + 3); // Convert PDT/PST to EDT/EST.
            startTimestamp = fromDate_(startDate);

            return {
                start: startTimestamp,
                end: endTimestamp,
                resolution: resolution
            };
        }
    }
} ]);

angular.module('marketTimer', [ 'timestamp', 'util' ])
       .factory('marketTimer', [ 'timestamp', 'util', function (timestamp, util) {
           let tradeHours = undefined;
           let lastIndex = 0;
           let count = 0;

           let extendedHours = true;

           let tickMap = {};
           let displayHours = false;

           return {
               init: function (tradeHours_, extendedHours_) {
                   tradeHours = tradeHours_;
                   count = tradeHours.length;
                   extendedHours = extendedHours_;
               },

               get: function (t) {
                   for (let i = lastIndex; i < count; ++i) {
                       let entry = tradeHours[ i ];

                       if (extendedHours) {
                           if ((entry.extStart <= t) && (t <= entry.extEnd)) {
                               lastIndex = i;
                               return entry.extSec + timestamp.getSeconds(t, entry.extStart);
                           }
                       }
                       else {
                           let open = timestamp.snapToMarketOpen(entry.extStart);
                           let close = timestamp.snapToMarketClose(entry.extEnd);
                           if ((open <= t) && (t <= close)) {
                               lastIndex = i;
                               return entry.stdSec + timestamp.getSeconds(t, open);
                           }
                       }
                   }

                   for (let i = 0; i < lastIndex; ++i) {
                       let entry = tradeHours[ i ];

                       if (extendedHours) {
                           if ((entry.extStart <= t) && (t <= entry.extEnd)) {
                               lastIndex = i;
                               return entry.extSec + timestamp.getSeconds(t, entry.extStart);
                           }
                       }
                       else {
                           let open = timestamp.snapToMarketOpen(entry.extStart);
                           let close = timestamp.snapToMarketClose(entry.extEnd);
                           if ((open <= t) && (t <= close)) {
                               lastIndex = i;
                               return entry.stdSec + timestamp.getSeconds(t, open);
                           }
                       }
                   }

                   return -9999;
               },

               generateTicks: function () {
                   let tickVals = [];

                   tickMap = {};

                   let startDate = timestamp.toDate(tradeHours[ 0 ].extStart);
                   let endDate = timestamp.toDate(tradeHours[ count - 1 ].extEnd);

                   displayHours = false;
                   let tickType = 0; // Tick on every Monday
                   if (endDate.getTime() - startDate.getTime() <= 3 * 24 * 60 * 60 * 1000) { // Less than 72 hours
                       tickType = 1; // Tick on every hour
                       displayHours = true;
                   }
                   else if (endDate.getTime() - startDate.getTime() < 60 * 24 * 60 * 60 * 1000) { // Less than a month
                       tickType = 2; // Tick on every day
                   }

                   let lastStartOfWeek = undefined;

                   for (let i = 0; i < count; ++i) {
                       let entry = tradeHours[ i ];
                       let d = timestamp.toDate(entry.extStart);
                       if (d.getDay() === 0) {
                           continue;
                       }

                       if (tickType === 0) {
                           if (lastStartOfWeek) {
                               if ((d.getTime() - lastStartOfWeek.getTime()) < 3 * 24 * 60 * 60 * 1000) {
                                   // Next tick should be at least 3 days away...
                                   continue;
                               }

                               if ((1 <= d.getDay()) && (d.getDay() <= 3)) {
                                   // This is usually Monday. If the market is closed on Monday, it is Tuesday.
                                   lastStartOfWeek = d;
                               }
                               else {
                                   continue;
                               }
                           }
                           else {
                               lastStartOfWeek = d;
                           }
                       }

                       if (extendedHours) {
                           let open = timestamp.toDate(entry.extStart);
                           tickVals.push(entry.extSec);
                           tickMap[ entry.extSec ] = open;

                           let close = timestamp.toDate(entry.extEnd);

                           if (tickType === 1) {
                               let s = entry.extSec;
                               let t = new Date(open);
                               while (true) {
                                   t.setHours(t.getHours() + 1);
                                   s += 3600;
                                   if (t >= close) {
                                       break;
                                   }
                                   tickVals.push(s);
                                   tickMap[ s ] = new Date(t);
                               }
                           }
                       }
                       else {
                           let open = timestamp.toDate(timestamp.snapToMarketOpen(entry.extStart));
                           tickVals.push(entry.stdSec);
                           tickMap[ entry.stdSec ] = open;

                           if (tickType === 1) {
                               let s = entry.stdSec;
                               let t = new Date(open);
                               while (t.getHours() < 15) {
                                   t.setHours(t.getHours() + 1);
                                   s += 3600;
                                   tickVals.push(s);
                                   tickMap[ s ] = new Date(t);
                               }
                           }
                       }
                   }

                   return tickVals;
               },

               tickFormat: function (val) {
                   let d = tickMap[ val ];
                   if (displayHours) {
                       if (d.getHours() === 9) {
                           return '[' + util.padInt(d.getMonth() + 1, 2) + '/' + util.padInt(d.getDate(), 2) + ']';
                       }
                       return util.padInt(d.getHours(), 2) + ':' + util.padInt(d.getMinutes(), 2);
                   }
                   let dateStr = util.padInt(d.getMonth() + 1, 2) + '/' + util.padInt(d.getDate(), 2);
                   if ((d.getMonth() === 0) && (d.getDate() === 1)) {
                       dateStr += ', ' + d.getFullYear();
                   }
                   return dateStr;
               },

               getExtendedHourZones: function () {
                   let zones = [];

                   for (let i = 0; i < count; ++i) {
                       let entry = tradeHours[ i ];
                       let day = timestamp.toDate(entry.extStart).getDay();

                       // Futures are open on Sunday. We consider all day as part of extended hours.
                       if (day === 0) {
                           if (i < count - 1) {
                               // The zone covers until Monday's open.
                               zones.push({
                                              start: entry.extStart,
                                              end: timestamp.snapToMarketOpen(tradeHours[ i + 1 ].extStart)
                                          });
                           }
                           else {
                               // This is the last entry. The zone ends at the end of the day.
                               zones.push({
                                              start: entry.extStart,
                                              end: entry.extEnd
                                          });
                           }

                           continue;
                       }

                       // Monday through Friday...

                       if (i === 0) {
                           zones.push({
                                          start: entry.extStart,
                                          end: timestamp.snapToMarketOpen(entry.extStart)
                                      });
                       }

                       if (i < count - 1) {
                           let nextDay = timestamp.toDate(tradeHours[ i + 1 ].extStart).getDay();
                           if (nextDay === 0) {
                               // The next day is Sunday. In this case, we need to extend the zone further to Monday's market open...
                               if (i < count - 2) {
                                   zones.push({
                                                  start: timestamp.snapToMarketClose(entry.extEnd),
                                                  end: timestamp.snapToMarketOpen(tradeHours[ i + 2 ].extStart)
                                              });
                               }
                               else {
                                   zones.push({
                                                  start: timestamp.snapToMarketClose(entry.extEnd),
                                                  end: tradeHours[ i + 1 ].extEnd
                                              });
                               }
                               ++i;
                           }
                           else {
                               // The zone covers from today's market close to tomorrow's market open.
                               zones.push({
                                              start: timestamp.snapToMarketClose(entry.extEnd),
                                              end: timestamp.snapToMarketOpen(tradeHours[ i + 1 ].extStart)
                                          });
                           }
                       }
                       else {
                           // This is the last entry. The zone ends at the close of today's exteded hours.
                           zones.push({
                                          start: timestamp.snapToMarketClose(entry.extEnd),
                                          end: entry.extEnd
                                      });
                       }
                   }

                   return zones;
               }
           }
       } ]);

angular.module('util', []).factory('util', [ '$window', function ($window) {
    let rgbToHsl = function (r, g, b) {
        r /= 255;
        g /= 255;
        b /= 255;

        let max = Math.max(r, g, b), min = Math.min(r, g, b);
        let h, s, l = (max + min) / 2;

        if (max === min) {
            h = s = 0; // achromatic
        }
        else {
            let d = max - min;
            s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
            switch (max) {
                case r:
                    h = (g - b) / d + (g < b ? 6 : 0);
                    break;
                case g:
                    h = (b - r) / d + 2;
                    break;
                case b:
                    h = (r - g) / d + 4;
                    break;
            }
            h /= 6;
        }

        return [ h, s, l ];
    };

    let hslToRgb = function (h, s, l) {
        let r;
        let g;
        let b;

        if (s === 0) {
            r = g = b = l; // achromatic
        }
        else {
            let hue2rgb = function hue2rgb(p, q, t) {
                if (t < 0) {
                    t += 1;
                }
                if (t > 1) {
                    t -= 1;
                }
                if (t < 1 / 6) {
                    return p + (q - p) * 6 * t;
                }
                if (t < 1 / 2) {
                    return q;
                }
                if (t < 2 / 3) {
                    return p + (q - p) * (2 / 3 - t) * 6;
                }
                return p;
            };

            let q = l < 0.5 ? l * (1 + s) : l + s - l * s;
            let p = 2 * l - q;
            r = hue2rgb(p, q, h + 1 / 3);
            g = hue2rgb(p, q, h);
            b = hue2rgb(p, q, h - 1 / 3);
        }

        return [ Math.round(r * 255), Math.round(g * 255), Math.round(b * 255) ];
    };

    let padInt_ = function (value, digits) {
        let d = '0'.repeat(digits);
        return (d + value).substr(-1 * digits, digits);
    };

    // @formatter:off
    let _12Pallete = [ "#6fcb4e",
                       "#9c82e9",
                       "#d7d54b",
                       "#4949ff", //"#6564ff",
                       "#de8136",
                       "#5ed2c4",
                       "#Ff5d51",
                       "#0d0cc4",
                       "#9f0b02",
                       "#106b26",
                       "#d1ac66",
                       "#9cab52" ];

    let _27Palette = [ "#336699", // 200
                       "#99CCFF", // 201
                       "#999933", // 202
                       "#666699", // 203
                       "#CC9933", // 204
                       "#006666", // 205
                       "#3399FF", // 206
                       "#993300", // 207
                       "#CCCC99", // 208
                       "#FFCC66", // 209
                       "#6699CC", // 210
                       "#663366", // 211
                       "#9999CC", // 212
                       "#669999", // 213
                       "#CCCC66", // 214
                       "#CC6600", // 215
                       "#9999FF", // 216
                       "#0066CC", // 217
                       "#99CCCC", // 218
                       "#FFCC00", // 219
                       "#009999", // 220
                       "#99CC33", // 221
                       "#FF9900", // 222
                       "#999966", // 223
                       "#66CCCC", // 224
                       "#339966", // 225
                       "#CCCC33"  // 226
    ];
    // @formatter:on

    return {
        parseOptionSymbol: function (symbol) {
            let optionRegExp = /^(\D*)([0-9]{2})([0-9]{2})([0-9]{2})([C|P])(\S*)/;
            let regRes = optionRegExp.exec(symbol);
            if (!regRes) {
                return null;
            }

            let year = parseInt('20' + regRes[ 2 ]);
            let month = parseInt(regRes[ 3 ]);
            let day = parseInt(regRes[ 4 ]);
            let date = new Date(year, month - 1, day);

            return {
                underlier: regRes[ 1 ],
                expireDate: date,
                optionType: regRes[ 5 ],
                strike: parseFloat(regRes[ 6 ]) / 100
            };
        },

        createOptionSymbol: function (symbol, expire, strike, type) {
            let y = expire.getFullYear() - 2000;
            let m = padInt_(expire.getMonth() + 1, 2);
            let d = padInt_(expire.getDate(), 2);

            return symbol + y + m + d + type + Math.round(strike * 100);
        }

        ,

        generateTicks: function (min, max) {
            max = parseFloat(max);
            min = parseFloat(min);

            let tickLog = Math.log10(Math.abs(max - min));
            let offset = tickLog - Math.floor(tickLog);
            if (offset > 0.2) {
                tickLog = Math.floor(tickLog);
            }
            else {
                tickLog = Math.floor(tickLog) - 1;
            }

            let tick = Math.pow(10, tickLog);
            if (((offset > 0.2)) && (offset < 0.5)) {
                tick /= 2;
            }

            let tickVals = [];
            let val = Math.floor(min / tick) * tick;

            let maxWithMargin = max + tick * 1.2;
            while (val <= maxWithMargin) {
                tickVals.push(val);
                val += tick;
            }

            return tickVals;
        },

        generateTicks2: function (min, max, logScale) {
            let yTickVals = [];
            let yTick = 0;

            if (!logScale) {
                let yDiff = max - min;

                let count = 3;
                let criteria = [ 500, 200, 100 ];
                let ticks = [ 100, 50, 20 ];

                while (yTick === 0) {
                    for (let i = 0; i < count; ++i) {
                        if (yDiff >= criteria[ i ]) {
                            yTick = ticks[ i ];
                            break;
                        }
                        criteria[ i ] = criteria[ i ] / 10;
                        ticks[ i ] = ticks[ i ] / 10;
                    }
                }

                min = Math.floor(min / yTick) * yTick;

                let maxNum = Math.max(Math.abs(max), Math.abs(min));
                let precision = Math.ceil(Math.log10(maxNum)) - Math.floor(Math.log10(yTick));

                let i = 0;
                let nextTick = min;
                while (nextTick <= max + yTick) {
                    yTickVals.push(parseFloat(nextTick.toPrecision(precision)));
                    ++i;
                    nextTick = min + yTick * i;
                }
            }
            else {
                let logY = min;
                let additionalLines = (max / min) < 100000;

                while (logY < max * 2) {
                    if (additionalLines) {
                        for (let j = 1; j < 9; ++j) {
                            yTickVals.push(logY * j);
                            if (logY > max) {
                                break;
                            }
                        }
                    }
                    else {
                        yTickVals.push(logY);
                    }
                    logY *= 10;
                }
            }

            return yTickVals;
        },

        padInt: function (value, digits) {
            return padInt_(value, digits);
        },

        numberWithCommas: function (x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },

        getColor: function (index) {
            if ((100 <= index) && (index < 200)) {
                index = (index - 100) % _12Pallete.length;
                return _12Pallete[ index ];
            }
            else {
                index = (index - 200) % _27Palette.length;
                return _27Palette[ index ];
            }

            index = index % 6;
            switch (index) {
                case 0:
                    return '#4f81bd';
                case 1:
                    return '#c0504d';
                case 2:
                    return '#9bbb59';
                case 3:
                    return '#8064a2';
                case 4:
                    return '#4bacc6';
                case 5:
                    return '#f79646';
                default:
                    return '#f79646';
            }
        },

        getColorEx: function (index, luminance) {
            let r;
            let g;
            let b;

            index = index % 6;
            switch (index) {
                case 0:
                    r = 0x4f;
                    g = 0x81;
                    b = 0xbd;
                    break;
                case 1:
                    r = 0xc0;
                    g = 0x50;
                    b = 0x4d;
                    break;
                case 2:
                    r = 0x9b;
                    g = 0xbb;
                    b = 0x59;
                    break;
                case 3:
                    r = 0x80;
                    g = 0x64;
                    b = 0xa2;
                    break;
                case 4:
                    r = 0x4b;
                    g = 0xac;
                    b = 0xc6;
                    break;
                case 5:
                    r = 0xf7;
                    g = 0x96;
                    b = 0x46;
                    break;
                default:
                    r = 0xf7;
                    g = 0x96;
                    b = 0x46;
                    break;
            }

            let hsl = rgbToHsl(r, g, b);
            let l = hsl[ 2 ] * luminance;
            if (l > 255) {
                l = 255;
            }
            let rgb = hslToRgb(hsl[ 0 ], hsl[ 1 ], l);

            return '#' + padInt_(rgb[ 0 ].toString(16), 2) + padInt_(rgb[ 1 ].toString(16), 2) + padInt_(rgb[ 2 ].toString(16), 2);
        }
    };
} ]);
